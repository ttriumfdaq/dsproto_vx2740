#ifndef CAEN_DATA_H
#define CAEN_DATA_H

#include "midas.h"
#include "caen_device.h"
#include "caen_parameters.h"
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <map>
#include <memory>
#include <queue>
#include <exception>

#define VX_NO_EVENT 150
#define VX_CALL_AGAIN 151

class CaenData {
   public:
      CaenData() {}
      ~CaenData() {}

      void set_device(std::shared_ptr<CaenDevice> _dev) {
         dev = _dev;
      }

      // Get raw data from board.
      // buffer should have enough space to hold at least get_max_raw_bytes_per_read() bytes.
      // convert_to_host_order=false does a direct memcpy
      // convert_to_host_order=true applies htonl() to all the incoming data, so is slower, but the data is easier to use.
      INT get_raw_data(int timeout_ms, uint8_t* buffer, size_t& num_bytes_read, bool convert_to_host_order=true);

      // Get decoded data for an event.
      // Waveforms should be pre-allocated as [64][NUM_SAMPLES].
      INT get_decoded_scope_data(int timeout_ms, uint64_t &timestamp, uint32_t &event_counter, uint16_t &event_flags, uint16_t** waveforms);

      // Get decoded data for an event.
      // Waveform should be pre-allocated as [MAX_NUM_SAMPLES].
      INT get_decoded_user_data(int timeout_ms, uint8_t& channel_id, uint64_t &timestamp, size_t& waveform_size, uint8_t& flagsA, uint16_t& flagsB, uint32_t& len, uint32_t& len_for_sum, uint64_t& sum, size_t& user_info_size, uint16_t* waveform, uint64_t* user_info);
      INT get_decoded_user_data_as_vector(int timeout_ms, uint8_t& channel_id, uint64_t &timestamp, uint8_t& flagsA, uint16_t& flagsB, uint32_t& len, uint32_t& len_for_sum, uint64_t& sum, std::vector<uint16_t>& waveform, std::vector<uint64_t>& user_info);

      // Encode data read by get_decoded_scope_data() into a buffer in the same format
      // as the prototype boards gave their data.
      // Returns the number of bytes written to buffer.
      uint32_t encode_scope_data_to_buffer(uint64_t chan_enable_mask, uint32_t wf_len_samples, uint64_t timestamp, uint32_t event_counter, uint16_t event_flags, uint16_t** waveforms, uint8_t* buffer);

      // Encode data read by get_decoded_user_data() into a buffer in the same format
      // as the prototype boards gave their data in SCOPE mode.
      // Returns the number of bytes written to buffer.
      uint32_t encode_user_data_to_buffer(uint8_t channel_id, uint64_t timestamp, size_t waveform_size, uint8_t flagsA, uint16_t* waveform, uint8_t* buffer);
      static uint32_t encode_user_data_to_buffer_static(uint8_t channel_id, uint64_t timestamp, size_t waveform_size, uint8_t flagsA, int event_counter, uint16_t* waveform, uint8_t* buffer);

      // Get a handle for reading waveform data.
      // Choose either raw or decoded data.
      // This must be called BEFORE starting acquisition!
      INT setup_data_handle(bool raw, CaenParameters& params);

      // Whether to try to stitch together longer waveforms from user mode FW
      // that were split into smaller chunks.
      void enable_stitching_for_user_data(bool enable, size_t max_samples, size_t fixed_samples, size_t max_user_info);

      bool is_end_of_slice(uint8_t flagsA, uint16_t flagsB);
      uint16_t get_decimation_factor(uint8_t flagsA, uint16_t flagsB);

      void get_stitching_params(bool& enabled, size_t& max_samples, size_t& max_user_info) {
         enabled = enable_stitching;
         max_samples = max_stitched_samples;
         max_user_info = max_stitched_user_info;
      }

      void set_debug(bool _debug) {
         debug = _debug;
      }

   protected:

      // Get decoded data for an event from the board.
      // Waveform should be pre-allocated as [MAX_NUM_SAMPLES].
      INT read_user_data_from_board(int timeout_ms, uint8_t& channel_id, uint64_t &timestamp, size_t& waveform_size, uint8_t& flagsA, uint16_t& flagsB, uint32_t& len, uint64_t& sum, size_t& user_info_size, uint16_t* waveform, uint64_t* user_info);
      INT get_decoded_user_data_impl(int timeout_ms, uint8_t& channel_id, uint64_t &timestamp, size_t& waveform_size, uint8_t& flagsA, uint16_t& flagsB, uint32_t& len, uint32_t& len_for_sum, uint64_t& sum, size_t& user_info_size, bool as_vectors, uint16_t* waveform, uint64_t* user_info, std::vector<uint16_t>& waveform_vec, std::vector<uint64_t>& user_info_vec);

      static const int VX_EXTEND_MAX_GAP = 4;

      std::shared_ptr<CaenDevice> dev = nullptr;
      bool debug = false;

      bool enable_stitching = false;
      size_t max_stitched_samples = 0x7FFF;
      size_t max_stitched_user_info = 0x100;

      int user_mode_event_count = 0;
      bool setup_raw_data = false;
      bool setup_decoded_scope_data = false;
      bool setup_decoded_user_data = false;
      uint64_t data_handle = 0;
      std::string fw_type;

      typedef struct Stitched {
         ~Stitched() {
            free_buffers();
         }

         void alloc_buffers(size_t wf_words, size_t ui_words) {
            free_buffers();
            waveform = (uint16_t*)calloc(wf_words, sizeof(uint16_t));
            user_info = (uint64_t*)calloc(ui_words, sizeof(uint64_t));
         }

         void free_buffers() {
            if (waveform != nullptr) {
               free(waveform);
               waveform = nullptr;
            }
            if (user_info != nullptr) {
               free(user_info);
               user_info = nullptr;
            }
         }

         void reset() {
            timestamp = 0;
            waveform_size = 0;
            decimation_factor = 1;
            flagsA = 0;
            flagsB = 0;
            len = 0;
            len_for_sum = 0;
            sum = 0;
            user_info_size = 0;
         }

         void copy_from(Stitched& src) {
            timestamp = src.timestamp;
            waveform_size = src.waveform_size;
            decimation_factor = src.decimation_factor;
            flagsA = src.flagsA;
            flagsB = src.flagsB;
            len = src.len;
            len_for_sum = src.len_for_sum;
            sum = src.sum;
            user_info_size = src.user_info_size;
            memcpy(waveform, src.waveform, waveform_size * sizeof(uint16_t));
            memcpy(user_info, src.user_info, user_info_size * sizeof(uint64_t));
         }

         uint32_t upsampled_waveform_size() {
            if (waveform_size == 0) {
               // TODO - check what len is when decimating!
               return len * decimation_factor;
            } else {
               return waveform_size * decimation_factor;
            }
         }

         void merge_from(Stitched& other) {
            flagsA |= other.flagsA;
            flagsB |= other.flagsB;

            len += other.len;
            len_for_sum += other.len_for_sum;
            sum += other.sum;

            int32_t this_gap = (other.timestamp - timestamp - upsampled_waveform_size()) / decimation_factor;

            if (this_gap <= 0) {
               // Copy waveform, ignoring duplicated samples
               memcpy(waveform + waveform_size, other.waveform - this_gap, (other.waveform_size + this_gap) * sizeof(uint16_t));
               waveform_size += other.waveform_size + this_gap;
            } else {
               // Interpolate missing samples
               uint16_t last_curr = waveform[waveform_size-1];
               uint16_t first_next = other.waveform[0];

               for (int i = 0; i < this_gap; i++) {
                  waveform[waveform_size + i] = last_curr + float(first_next - last_curr) * float(i/this_gap);
               }

               memcpy(waveform + waveform_size + this_gap, other.waveform, other.waveform_size * sizeof(uint16_t));
               waveform_size += other.waveform_size + this_gap;
            }
         
            memcpy(user_info + user_info_size, other.user_info, other.user_info_size * sizeof(uint64_t));
            user_info_size += other.user_info_size;
         }

         uint64_t timestamp = 0;
         size_t waveform_size = 0;
         uint8_t decimation_factor = 1;
         uint8_t flagsA = 0;
         uint16_t flagsB = 0;
         uint32_t len = 0;
         uint32_t len_for_sum = 0;
         uint64_t sum = 0;
         size_t user_info_size = 0;
         uint16_t* waveform = nullptr;
         uint64_t* user_info = nullptr;
      } Stitched;

      uint8_t stitch_pending_chan = -1;
      Stitched stitch_pending = {};
      Stitched stitched[64] = {};
      std::queue<int> stitched_complete;

      std::vector<uint16_t> dummy_waveform_vec;
      std::vector<uint64_t> dummy_user_info_vec;
};

#endif
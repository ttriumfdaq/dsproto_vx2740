#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <iostream>
#include <cstdio>
#include <sys/time.h>
#include <stdarg.h>
#include <pthread.h>
#include "midas.h"
#include "msystem.h"
#include "fe_settings_strategy.h"
#include "vx2740_fe_class.h"

#undef NDEBUG // midas required assert() to be always enabled
#include "tmfe.h"

#define MAX_EV_SIZE 100000000  // 100MB

// Object for interacting with registers.
// Here we specify to use "group fe mode"
VX2740GroupFrontend vx_group(std::make_shared<VX2740FeSettingsODB>(), false);

class EqErrors : public TMFeEquipment {
public:
   EqErrors(const char* eqname, const char* eqfilename)
      : TMFeEquipment(eqname, eqfilename) {
      fEqConfReadConfigFromOdb = false;
      fEqConfEventID = 105;
      fEqConfBuffer = "BUFVXERR";
      fEqConfPeriodMilliSec = 1000; // in milliseconds
      fEqConfReadOnlyWhenRunning = false;
      fEqConfLogHistory = 0;
      fEqConfWriteEventsToOdb = false;

      buf = (char*)malloc(1024);
   }

   virtual ~EqErrors() {
      free(buf);
   }

   void HandlePeriodic() {
      ComposeEvent(buf, sizeof(buf));
      
      if (vx_group.check_errors(buf + sizeof(EVENT_HEADER)) > 0) {
         EqSendEvent(buf);
      }
   }

private:
   char* buf = nullptr;
};

class EqConfig : public TMFeEquipment {
public:
   EqConfig(const char* eqname, const char* eqfilename)
      : TMFeEquipment(eqname, eqfilename) {
      fEqConfReadConfigFromOdb = false;
      fEqConfEventID = 103;
      fEqConfBuffer = "BUFVXCONF";
      fEqConfPeriodMilliSec = 6000; // in milliseconds
      fEqConfReadOnlyWhenRunning = true;
      fEqConfLogHistory = 1;
      fEqConfWriteEventsToOdb = true;

      buf = (char*)malloc(10240);
   }

   virtual ~EqConfig() {
      free(buf);
   }

   void HandlePeriodic() {
      ComposeEvent(buf, sizeof(buf));
      
      if (vx_group.write_metadata(buf + sizeof(EVENT_HEADER)) > 0) {
         EqSendEvent(buf);
      }
   }

   TMFeResult HandleBeginRun(int run_number) {
      char error[1024];
      INT status = vx_group.begin_of_run(run_number, error);

      if (status == SUCCESS) {
         return TMFeOk();
      } else {
         return TMFeResult(status, error);
      }
   }

   TMFeResult HandleEndRun(int run_number) {
      char error[1024];
      INT status = vx_group.end_of_run(run_number, error);

      if (status == SUCCESS) {
         return TMFeOk();
      } else {
         return TMFeResult(status, error);
      }
   }

   TMFeResult HandleStartAbortRun(int run_number) {
      return HandleEndRun(run_number);
   }

private:
   char* buf = nullptr;
};

class EqData : public TMFeEquipment {
public:
   EqData(const char* eqname, const char* eqfilename)
      : TMFeEquipment(eqname, eqfilename) {
      fEqConfReadConfigFromOdb = false;
      fEqConfEventID = 104;
      fEqConfBuffer = "SYSTEM";
      fEqConfEnablePoll = true;
      fEqConfReadOnlyWhenRunning = true;
      fEqConfLogHistory = 0;
      fEqConfWriteEventsToOdb = false;

      buf = (char*)malloc(MAX_EV_SIZE);
   }

   virtual ~EqData() {
      free(buf);
   }

   TMFeResult HandleInit(const std::vector<std::string>& args) {
      // Multi-threaded results in race condition? When check_errors() tries
      // to write to ODB?
      //EqStartPollThread();
      return TMFeOk();
   }

   bool HandlePoll() {
      return vx_group.is_event_ready();
   }

   void HandlePollRead() {
      ComposeEvent(buf, sizeof(buf));
      
      if (vx_group.write_data(buf + sizeof(EVENT_HEADER)) > 0) {
         EqSendEvent(buf);
      }
   }

private:
   char* buf = nullptr;
};

class FeVxGroup: public TMFrontend {
public:
   FeVxGroup() {
      /* register with the framework */
      FeSetName("VX2740_Group_%02d");
      FeAddEquipment(new EqErrors("VX2740_Errors_Group_%03d", __FILE__));
      FeAddEquipment(new EqConfig("VX2740_Config_Group_%03d", __FILE__));
      FeAddEquipment(new EqData("VX2740_Data_Group_%03d", __FILE__));

      fFeIndex = -1;
   }

   TMFeResult HandleFrontendInit(const std::vector<std::string>& args) {
      if (fFeIndex < 0) {
         cm_msg(MERROR, __FUNCTION__, "You must run this frontend with the -i argument to specify the frontend index.");
         return TMFeResult(FE_ERR_DRIVER, "You must run this frontend with the -i argument to specify the frontend index.");
      }

      INT status = vx_group.init(fFeIndex, fMfe->fDB);

      if (status == SUCCESS) {
         return TMFeOk();
      } else {
         return TMFeResult(status, "Failed to initialize frontend");
      }
   }

   TMFeResult HandleFrontendReady(const std::vector<std::string>& args) {
      FeStartPeriodicThread();
      return TMFeOk();
   }

};

int main(int argc, char* argv[])
{
   FeVxGroup fe_example;
   return fe_example.FeMain(argc, argv);
}

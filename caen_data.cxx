#include "caen_data.h"
#include "caen_event.h"
#include "caen_exceptions.h"
#include "CAEN_FELib.h"
#include "fe_utils.h"
#include "midas.h"
#include "inttypes.h"
#include <arpa/inet.h>
#include <string.h>
#include <cmath>

INT CaenData::setup_data_handle(bool raw, CaenParameters& params) {
   user_mode_event_count = 0;
   params.get_firmware_type(fw_type);

   std::string endpoint, format, activeendpoint;

   if (raw) {
      endpoint = "/endpoint/raw";
      format = "";
      activeendpoint = "raw";
   } else {
      if (fw_type == "Scope") {
         endpoint = "/endpoint/scope";
         activeendpoint = "scope";
         format = "[ { \"name\" : \"TIMESTAMP\", \"type\" : \"U64\" }, \
                     { \"name\" : \"TRIGGER_ID\", \"type\" : \"U32\" }, \
                     { \"name\" : \"WAVEFORM\", \"type\" : \"U16\", \"dim\" : 2 }, \
                     { \"name\" : \"WAVEFORM_SIZE\", \"type\" : \"U32\", \"dim\" : 1 }, \
                     { \"name\" : \"FLAGS\", \"type\" : \"U16\" } \
                   ]";
      } else {
         endpoint = "/endpoint/opendpp";
         activeendpoint = "opendpp";
         format = "[ { \"name\" : \"CHANNEL\", \"type\" : \"U8\" }, \
                     { \"name\" : \"TIMESTAMP\", \"type\" : \"U64\" }, \
                     { \"name\" : \"WAVEFORM_SIZE\", \"type\" : \"SIZE_T\" }, \
                     { \"name\" : \"FLAGS_A\", \"type\" : \"U8\" }, \
                     { \"name\" : \"FLAGS_B\", \"type\" : \"U16\" }, \
                     { \"name\" : \"ENERGY\", \"type\" : \"U16\" }, \
                     { \"name\" : \"FINE_TIMESTAMP\", \"type\" : \"U16\" }, \
                     { \"name\" : \"PSD\", \"type\" : \"U16\" }, \
                     { \"name\" : \"USER_INFO_SIZE\", \"type\" : \"SIZE_T\" }, \
                     { \"name\" : \"WAVEFORM\", \"type\" : \"U16\", \"dim\" : 1 }, \
                     { \"name\" : \"USER_INFO\", \"type\" : \"U64\", \"dim\" : 1 } \
                   ]";
      }
   }

   // Get the data handle
   if (CAEN_FELib_GetHandle(dev->get_root_handle(), endpoint.c_str(), &data_handle) != CAEN_FELib_Success) {
      dev->print_last_error("Failed to get data endpoint handle\n");
      return FE_ERR_DRIVER;
   }

   // Configure whether to give raw or decoded data
   try {
      params.set("/endpoint/par/activeendpoint", activeendpoint);
   } catch (CaenException& e) {
      dev->print_last_error("Failed to set active endpoint. You may need to upgrade to Dig2 library version >= 1.2.0 and/or upgrade your firmware\n");
      return FE_ERR_DRIVER;
   }

   if (CAEN_FELib_SetReadDataFormat(data_handle, format.c_str()) != CAEN_FELib_Success) {
      dev->print_last_error("Failed to set data format");
      return FE_ERR_DRIVER;
   }

   if (raw) {
      setup_raw_data = true;
      setup_decoded_scope_data = false;
      setup_decoded_user_data = false;
   } else if (fw_type == "Scope") {
      setup_raw_data = false;
      setup_decoded_scope_data = true;
      setup_decoded_user_data = false;
   } else {
      setup_raw_data = false;
      setup_decoded_scope_data = false;
      setup_decoded_user_data = true;
   }

   return SUCCESS;
}

void CaenData::enable_stitching_for_user_data(bool enable, size_t max_samples, size_t fixed_samples, size_t max_user_info) {
   enable_stitching = enable;

   max_stitched_samples = max_samples;
   max_stitched_user_info = max_user_info;
   size_t buf_samples = fixed_samples > max_samples ? fixed_samples : max_samples;

   for (int i = 0; i < 64; i++) {
      stitched[i].free_buffers();
      stitched[i].reset();

      if (enable_stitching) {
         stitched[i].alloc_buffers(buf_samples, max_stitched_user_info);
      }
   }

   stitch_pending_chan = 255;
   stitch_pending.free_buffers();
   stitch_pending.reset();

   if (enable_stitching) {
      stitch_pending.alloc_buffers(buf_samples, max_stitched_user_info);
   }

   while (!stitched_complete.empty()) {
      stitched_complete.pop();
   }
}

INT CaenData::get_raw_data(int timeout_ms, uint8_t* buffer, size_t& num_bytes_read, bool convert_to_host_order) {
   if (!setup_raw_data) {
      return FE_ERR_DRIVER;
   }

   int ret = CAEN_FELib_ReadData(data_handle, timeout_ms, buffer, &num_bytes_read);

   if (ret == CAEN_FELib_Success) {
      if (convert_to_host_order) {
         uint64_t* buf64 = (uint64_t*) buffer;
         uint32_t* buf32 = (uint32_t*) buffer;

         for (int i = 0; i < num_bytes_read/sizeof(uint64_t); i++) {
            buf64[i] = (((uint64_t)htonl(buf32[i * 2])) << 32) | htonl(buf32[i * 2 + 1]);
         }
      }
      return SUCCESS;
   } else if (ret == CAEN_FELib_Timeout) {
      return VX_NO_EVENT;
   } else {
      return FE_ERR_DRIVER;
   }
}

INT CaenData::get_decoded_scope_data(int timeout_ms, uint64_t &timestamp, uint32_t &event_counter, uint16_t &event_flags, uint16_t** waveforms) {
   if (!setup_decoded_scope_data) {
      return FE_ERR_DRIVER;
   }

   uint32_t waveform_sizes[64];
   int ret = CAEN_FELib_ReadData(data_handle, timeout_ms, &timestamp, &event_counter, waveforms, waveform_sizes, &event_flags);

   if (ret == CAEN_FELib_Success) {
      return SUCCESS;
   } else if (ret == CAEN_FELib_Timeout) {
      return VX_NO_EVENT;
   } else {
      return FE_ERR_DRIVER;
   }
}

INT CaenData::get_decoded_user_data(int timeout_ms, uint8_t& channel_id, uint64_t &timestamp, size_t& waveform_size, uint8_t& flagsA, uint16_t& flagsB, uint32_t& len, uint32_t& len_for_sum, uint64_t& sum, size_t& user_info_size, uint16_t* waveform, uint64_t* user_info) {
   return get_decoded_user_data_impl(timeout_ms, channel_id, timestamp, waveform_size, flagsA, flagsB, len, len_for_sum, sum, user_info_size, false, waveform, user_info, dummy_waveform_vec, dummy_user_info_vec);
}

INT CaenData::get_decoded_user_data_as_vector(int timeout_ms, uint8_t& channel_id, uint64_t &timestamp, uint8_t& flagsA, uint16_t& flagsB, uint32_t& len, uint32_t& len_for_sum, uint64_t& sum, std::vector<uint16_t>& waveform, std::vector<uint64_t>& user_info) {
   size_t waveform_size = 0xF000;
   size_t user_info_size = 0x100;
   return get_decoded_user_data_impl(timeout_ms, channel_id, timestamp, waveform_size, flagsA, flagsB, len, len_for_sum, sum, user_info_size, true, nullptr, nullptr, waveform, user_info);
}

bool CaenData::is_end_of_slice(uint8_t flagsA, uint16_t flagsB) {
   return flagsA & 0x1;
}

uint16_t CaenData::get_decimation_factor(uint8_t flagsA, uint16_t flagsB) {
   if ((flagsA >> 1) & 0x1) {
      uint8_t lg2 = ((flagsA >> 2) & 0xF);
      uint8_t factor = pow(2, lg2);
      return factor;
   }

   return 1;
}

INT CaenData::get_decoded_user_data_impl(int timeout_ms, uint8_t& channel_id, uint64_t &timestamp, size_t& waveform_size, uint8_t& flagsA, uint16_t& flagsB, uint32_t& len, uint32_t& len_for_sum, uint64_t& sum, size_t& user_info_size, bool as_vectors, uint16_t* waveform, uint64_t* user_info, std::vector<uint16_t>& waveform_vec, std::vector<uint64_t>& user_info_vec) {
   if (!enable_stitching) {
      if (as_vectors) {
         waveform_vec.resize(0xF000);
         user_info_vec.resize(0x200);
      }

      uint16_t* dest_wf = as_vectors ? waveform_vec.data() : waveform;
      uint64_t* dest_ui = as_vectors ? user_info_vec.data() : user_info;
      INT retval = read_user_data_from_board(timeout_ms, channel_id, timestamp, waveform_size, flagsA, flagsB, len, sum, user_info_size, dest_wf, dest_ui);
      
      len_for_sum = len - 3;

      if (as_vectors) {
         waveform_vec.resize(waveform_size);
         user_info_vec.resize(user_info_size);
      }
      
      return retval;
   }
   
   if (!stitched_complete.empty()) {
      // Fill data from waveforms we've completed.
      channel_id = stitched_complete.front();
      stitched_complete.pop();

      timestamp = stitched[channel_id].timestamp;
      waveform_size = stitched[channel_id].waveform_size;
      flagsA = stitched[channel_id].flagsA;
      flagsB = stitched[channel_id].flagsB;
      len = stitched[channel_id].len;
      len_for_sum = stitched[channel_id].len_for_sum;
      sum = stitched[channel_id].sum;
      user_info_size = stitched[channel_id].user_info_size;

      if (as_vectors) {
         waveform_vec.resize(waveform_size);
         user_info_vec.resize(user_info_size);
      }

      uint16_t* dest_wf = as_vectors ? waveform_vec.data() : waveform;
      uint64_t* dest_ui = as_vectors ? user_info_vec.data() : user_info;

      memcpy(dest_wf, stitched[channel_id].waveform, waveform_size * sizeof(uint16_t));
      memcpy(dest_ui, stitched[channel_id].user_info, user_info_size * sizeof(uint64_t));

      stitched[channel_id].reset();

      if (debug) {
         fe_utils::ts_printf("Returning completely stitched event from channel %u at timestamp %" PRIu64 " with %u samples\n", channel_id, timestamp, waveform_size);
      }

      return SUCCESS;
   } 
   
   if (stitch_pending_chan <= 64) {
      if (debug) {
         fe_utils::ts_printf("Handling pending stitch from channel %u\n", stitch_pending_chan);
      }

      stitched[stitch_pending_chan].copy_from(stitch_pending);
      stitch_pending_chan = 255;
   }
   
   // Read our next batch of data
   INT status = read_user_data_from_board(timeout_ms, stitch_pending_chan, stitch_pending.timestamp, stitch_pending.waveform_size, stitch_pending.flagsA, stitch_pending.flagsB, stitch_pending.len, stitch_pending.sum, stitch_pending.user_info_size, stitch_pending.waveform, stitch_pending.user_info);

   if (status != SUCCESS) {
      return status;
   }

   stitch_pending.decimation_factor = get_decimation_factor(stitch_pending.flagsA, stitch_pending.flagsB);

   // FW misses the first 3 samples of each waveform when calculating sum...
   stitch_pending.len_for_sum = stitch_pending.len - 3;

   if (debug) {
      fe_utils::ts_printf("See event from channel %u at timestamp %" PRIu64 " with %u samples (after decimation of factor %u). Summary len/len/sum %u/%u/%u\n", stitch_pending_chan, stitch_pending.timestamp, stitch_pending.waveform_size, stitch_pending.decimation_factor, stitch_pending.len, stitch_pending.len_for_sum, stitch_pending.sum);
   }

   bool is_special_event = is_end_of_slice(stitch_pending.flagsA, stitch_pending.flagsB);
   status = VX_CALL_AGAIN;

   int32_t gap_to_pending = stitch_pending.timestamp - stitched[stitch_pending_chan].timestamp - stitched[stitch_pending_chan].upsampled_waveform_size();
   
   // We've got some data to consider for stitching...
   if (stitched[stitch_pending_chan].timestamp == 0) {
      // New entry for this channel
      if (debug) {
         fe_utils::ts_printf("Starting new stitch for channel %u\n", stitch_pending_chan);
      }
      
      stitched[stitch_pending_chan].copy_from(stitch_pending);
      stitch_pending_chan = 255;
   } else if (!is_special_event &&
              gap_to_pending <= VX_EXTEND_MAX_GAP &&
              ((stitch_pending.waveform_size > 0 && stitched[stitch_pending_chan].waveform_size > 0) || (stitch_pending.waveform_size == 0 && stitched[stitch_pending_chan].waveform_size == 0)) &&
              stitch_pending.waveform_size + stitched[stitch_pending_chan].waveform_size < max_stitched_samples &&
              stitch_pending.user_info_size + stitched[stitch_pending_chan].user_info_size < max_stitched_user_info) {
      // Append to existing stitch for this channel
      if (debug) {
         fe_utils::ts_printf("Appending %u samples to stitch for channel %u\n", stitch_pending.waveform_size, stitch_pending_chan);
      }
      
      stitched[stitch_pending_chan].merge_from(stitch_pending);
      stitch_pending_chan = 255;
   } else {
      // New entry for this channel but we need to flush the previous entry first
      if (debug) {
         if (is_special_event) {
            fe_utils::ts_printf("Is end-of-slice event. Will finalise the current state and pend starting a new one for channel %u.\n", stitch_pending_chan);
         } else if ((stitch_pending.waveform_size == 0 && stitched[stitch_pending_chan].waveform_size > 0) || (stitch_pending.waveform_size > 0 && stitched[stitch_pending_chan].waveform_size == 0)) {
            fe_utils::ts_printf("Don't want to mix waveform and summary-only segments. Will finalise the current state and pend starting a new one for channel %u.\n", stitch_pending_chan);
         } else if (gap_to_pending > VX_EXTEND_MAX_GAP) {
            fe_utils::ts_printf("Channel %u has gap %d samples from previous. Finalise previous and pend starting a new one.\n", stitch_pending_chan, gap_to_pending);
         } else if (stitch_pending.waveform_size + stitched[stitch_pending_chan].waveform_size >= max_stitched_samples) {
            fe_utils::ts_printf("Overall waveform would grow too large (%u > %u) if we appended these samples. Will finalise the current state and pend starting a new one for channel %u.\n", stitch_pending.waveform_size + stitched[stitch_pending_chan].waveform_size, max_stitched_samples, stitch_pending_chan);
         } else if (stitch_pending.user_info_size + stitched[stitch_pending_chan].user_info_size >= max_stitched_user_info) {
            fe_utils::ts_printf("Overall user info block would grow too large (%u > %u) if we appended info from this segment. Will finalise the current state and pend starting a new one for channel %u.\n", stitch_pending.user_info_size + stitched[stitch_pending_chan].user_info_size, max_stitched_user_info, stitch_pending_chan);
         } else {
            fe_utils::ts_printf("Need to pend a new stitch for channel %u, but we don't have detailed debug info for why.\n", stitch_pending_chan);
         }
      }
      
      stitched_complete.push(stitch_pending_chan);
   }

   // Check if other channels can be marked as complete (and will be
   // sent out before we read any more data in).
   for (int i = 0; i < 64; i++) {
      if (stitched[i].timestamp == 0 || i == stitch_pending_chan) {
         continue;
      }

      if (is_end_of_slice(stitched[i].flagsA, stitched[i].flagsB)) {
         if (debug) {
            fe_utils::ts_printf("Marking TSM event as complete for channel %u\n", i);
         }

         stitched_complete.push(i);
      } else if (stitch_pending.timestamp > stitched[i].timestamp + stitched[i].upsampled_waveform_size() + VX_EXTEND_MAX_GAP) {
         if (debug) {
            fe_utils::ts_printf("Marking stitched event as complete for channel %u\n", i);
         }

         stitched_complete.push(i);
      }
   }

   // Return either SUCCESS (for a special event) or VX_CALL_AGAIN (if we just stitched something).
   return status;
}

INT CaenData::read_user_data_from_board(int timeout_ms, uint8_t& channel_id, uint64_t &timestamp, size_t& waveform_size, uint8_t& flagsA, uint16_t& flagsB, uint32_t& len, uint64_t& sum, size_t& user_info_size, uint16_t* waveform, uint64_t* user_info) {
   if (!setup_decoded_user_data) {
      return FE_ERR_DRIVER;
   }

   // CAEN use 42 bits for energy (16), fine_timing (10), PSD (16).
   // We use the same 42 bits for len (12) and sum (30).
   uint16_t en = 0, ft = 0, psd = 0; 

   int ret = CAEN_FELib_ReadData(data_handle, timeout_ms, &channel_id, &timestamp, &waveform_size, &flagsA, &flagsB, &en, &ft, &psd, &user_info_size, waveform, user_info);

   if (ret == CAEN_FELib_Success) {
      uint64_t lq = (((uint64_t)psd) << 26) | ((uint64_t)(ft & 0x3FF) << 16) | en;
      // 12 bits in words. * 4 for 16 bit samples in 64 bit words.
      len = ((lq >> 30) & 0xFFF) * 4; 
      // 30 bits
      sum = lq & 0x3FFFFFFF; 
      return SUCCESS;
   } else if (ret == CAEN_FELib_Timeout) {
      return VX_NO_EVENT;
   } else {
      return FE_ERR_DRIVER;
   }
}

uint32_t CaenData::encode_scope_data_to_buffer(uint64_t chan_enable_mask, uint32_t wf_len_samples, uint64_t timestamp, uint32_t event_counter, uint16_t event_flags, uint16_t** waveforms, uint8_t* buffer) {
   // Encode into buffer in same format as prototype VX2740 did.
   CaenEventHeader header;
   header.ch_enable_mask = chan_enable_mask;

   int num_chan = 0;

   for (int i = 0; i < 64; i++) {
      if (header.ch_enable_mask & ((uint64_t)1) << i) {
         num_chan++;
      }
   }

   header.event_counter = event_counter;
   header.flags = event_flags;
   header.format = 0x10;
   header.overlap = 0;
   header.size_64bit_words = CaenEventHeader::num_header_words + num_chan * (wf_len_samples/4);
   header.trigger_time = timestamp;

   uint64_t* dwp = (uint64_t*)buffer;
   header.hencode(dwp);
   dwp += CaenEventHeader::num_header_words;

   for (int s = 0; s < wf_len_samples; s += 4) {
      for (int c = 0; c < 64; c++) {
         if (header.ch_enable_mask & ((uint64_t)1) << c) {
            uint64_t samp_dc = htonl(((uint32_t)(waveforms[c][s+3]) << 16) | (waveforms[c][s+2]));
            uint64_t samp_ba = htonl(((uint32_t)(waveforms[c][s+1]) << 16) | (waveforms[c][s]));

            *dwp++ = (samp_dc << 32) | samp_ba;
         }
      }
   }

   return header.size_bytes();
}

uint32_t CaenData::encode_user_data_to_buffer_static(uint8_t channel_id, uint64_t timestamp, size_t waveform_size, uint8_t flagsA, int event_counter, uint16_t* waveform, uint8_t* buffer) {
   // Encode into buffer in same format as prototype VX2740 did.
   CaenEventHeader header;
   header.ch_enable_mask = ((uint64_t)1) << channel_id;

   header.event_counter = event_counter;
   header.flags = flagsA;
   header.format = 0x10;
   header.overlap = 0;
   header.size_64bit_words = CaenEventHeader::num_header_words + (waveform_size/4);
   header.trigger_time = timestamp;

   uint64_t* dwp = (uint64_t*)buffer;
   header.hencode(dwp);
   dwp += CaenEventHeader::num_header_words;

   // User data is already in correct byte order.
   memcpy((void*)dwp, (void*)waveform, waveform_size*sizeof(uint16_t));

   return header.size_bytes();
}

uint32_t CaenData::encode_user_data_to_buffer(uint8_t channel_id, uint64_t timestamp, size_t waveform_size, uint8_t flagsA, uint16_t* waveform, uint8_t* buffer) {
   return encode_user_data_to_buffer_static(channel_id, timestamp, waveform_size, flagsA, user_mode_event_count++, waveform, buffer);
}

import numpy as np
import struct

#generate the pattern
pattern = np.zeros((64, 1024), dtype=np.uint16)
pattern[:, 0:400] = 1
pattern[:, 400:500] = 0
pattern[:, 500:1024] = 1

#open the file
with open('files/pattern.bin', 'wb') as f:
    for line in pattern:
        for sample in line:
            f.write(struct.pack('H', sample))
            
print('Pattern file written')

#read all the data back: one digit per uint16_t
with open('files/pattern.bin', 'rb') as f:
    data = f.read()
    data = struct.unpack('H' * 64 * 1024, data)
    data = np.array(data).reshape(64, 1024)
    np.set_printoptions(threshold=np.inf)
    print(data)
    print(f"Number of lines: {data.shape[0]}")
    print(f"Number of samples per line: {data.shape[1]}")

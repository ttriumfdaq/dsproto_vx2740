#include "caen_commands.h"
#include "CAEN_FELib.h"

INT CaenCommands::start_acq(bool sw_start) {
   int ret = dev->run_cmd("armacquisition");
   
   if (ret == SUCCESS && sw_start) {
      ret = dev->run_cmd("swstartacquisition");
   }

   return ret;
}

INT CaenCommands::stop_acq() {
   return dev->run_cmd("disarmacquisition");
}

INT CaenCommands::reset() {
   return dev->run_cmd("reset");
}

INT CaenCommands::clear_data() {
   return dev->run_cmd("cleardata");
}

INT CaenCommands::issue_software_trigger() {
   return dev->run_cmd("sendswtrigger");
}
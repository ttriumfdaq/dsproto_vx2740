#include <stdlib.h>
#include <inttypes.h>
#include <stdio.h>
#include <cstring>
#include <algorithm>
#include <arpa/inet.h>
#include "midas.h"
#include "CAEN_FELib.h"
#include "avoid_gcc_bug67791.h"
#include "caen_event.h"
#include "caen_exceptions.h"
#include "vx2740_wrapper.h"

INT VX2740::connect(std::string hostname, bool do_reset, bool monitor_only) {
   // Avoid bug that can cause us to segfault when using gcc.
   avoid_gcc_bug67791::go();

   dev = std::make_shared<CaenDevice>();
   dev->connect(hostname, do_reset, monitor_only);

   params_helper.set_device(dev);
   data_helper.set_device(dev);
   commands_helper.set_device(dev);

   std::string model_name, serial_num, fw_ver, fw_type;

   try {
      params_helper.get_firmware_type(fw_type);
      params_helper.get_firmware_version(fw_ver);
      params_helper.get_model_name(model_name);
      params_helper.get_serial_number(serial_num);
   } catch (CaenException& e) {
      return FE_ERR_DRIVER;
   }

   // Prefix:     VX - VME form factor      DT - desktop form factor
   // Number:   2740 - regular            2745 - variable VGA gain
   // Suffix: (none) - differential          B - single-ended
   std::vector<std::string> allowed_models;
   allowed_models.push_back("VX2740");
   allowed_models.push_back("VX2745");
   allowed_models.push_back("DT2740");
   allowed_models.push_back("DT2745");
   allowed_models.push_back("VX2740B");
   allowed_models.push_back("VX2745B");
   allowed_models.push_back("DT2740B");
   allowed_models.push_back("DT2745B");

   if (std::find(allowed_models.begin(), allowed_models.end(), model_name) == allowed_models.end()) {
      printf("Connected to a model %s not a VX/DT 2740/2745!", model_name.c_str());
      return FE_ERR_DRIVER;
   }

   printf("Connected to %s #%s, running %s FW version %s\n", model_name.c_str(), serial_num.c_str(), fw_type.c_str(), fw_ver.c_str());

   return SUCCESS;
}

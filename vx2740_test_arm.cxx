#include "CAEN_FELib.h"
#include "stdio.h"
#include <string>
#include <thread>
#include <sys/time.h>
#include <unistd.h>
#include <stdexcept>

/** This program is designed to help debug an issue where we may hang trying to arm a board.
  * 
  * The flow of the program is:
  *   > Connect to the boards and configure some sensible defaults
  *   > Spawn threads to arm the boards.
  *   > If the boards are armed okay: join the threads, disarm the boards, wait a bit, then try again.
  *   > If the boards are not armed: throw an exception that can be studied in a debugger.
  * 
  * You may need to run the program multiple times to hit the error condition. But when you do,
  * you'll see one of the threads stuck waiting for a response to the "/cmd/armacquisition" command.
  */

bool gArmed1 = false;
bool gArmed2 = false;



void usage(char *prog_name) {
   printf("Usage: %s <device_path> [<device_path>]\n", prog_name);
   printf("E.g. : %s Dig2:vx02 Dig2:vx03\n", prog_name);
}

void throw_error(std::string prefix, bool with_caen_err_msg=true) {
   char caen_msg[1024];
   char full_msg[2048];

   if (with_caen_err_msg) {
      int got_err_msg = CAEN_FELib_GetLastError(caen_msg);

      if (got_err_msg == CAEN_FELib_Success) {
         snprintf(full_msg, 2040, "%s: %s\n", prefix.c_str(), caen_msg);
      } else {
         snprintf(full_msg, 2040, "%s: Failed to get error message from CAEN FELib.\n", prefix.c_str());
      }

      throw std::runtime_error(full_msg);
   } else {
      throw std::runtime_error(prefix.c_str());
   }
}

void configure(uint64_t dev_handle) {
   // Fails with either raw or decoded endpoint, but using the raw endpoint means there's one
   // less thread to worry about.
   if (CAEN_FELib_SetValue(dev_handle, "/endpoint/par/activeendpoint", "raw") != CAEN_FELib_Success) {
      throw_error("Failed to set active endpoint\n");
   }

   if (CAEN_FELib_SetValue(dev_handle, "/par/StartSource", "SWcmd") != CAEN_FELib_Success) {
      throw_error("Failed to set start source\n");
   }
}

void arm(uint64_t dev_handle, bool with_sw_start=false) {
   if (CAEN_FELib_SendCommand(dev_handle, "/cmd/armacquisition") != CAEN_FELib_Success) {
      throw_error("Failed to arm board");
   }

   if (with_sw_start) {
      // Fails whether you send the SW start command or not.
      if (CAEN_FELib_SendCommand(dev_handle, "/cmd/swstartacquisition") != CAEN_FELib_Success) {
         throw_error("Failed to start board");
      }
   }
}

void disarm(uint64_t dev_handle) {
   if (CAEN_FELib_SendCommand(dev_handle, "/cmd/disarmacquisition") != CAEN_FELib_Success) {
      throw_error("Failed to disarm board");
   }
}

void *arm_thread(void *arg, bool* armed) {
   uint64_t dev_handle = *(uint64_t*)arg;
   arm(dev_handle);
   *armed = true;
   return NULL;
}

int main(int argc, char **argv) {
   if (argc != 2 && argc != 3) {
      usage(argv[0]);
      return 0;
   }

   int num_boards = argc - 1;
   uint64_t dev_handle_1, dev_handle_2;

   if (CAEN_FELib_Open(argv[1], &dev_handle_1) != CAEN_FELib_Success) {
      throw_error("Failed to connect to first board");
   }

   configure(dev_handle_1);

   if (num_boards == 2) {
      if (CAEN_FELib_Open(argv[2], &dev_handle_2) != CAEN_FELib_Success) {
         throw_error("Failed to connect to second board");
      }

      configure(dev_handle_2);
   }

   int num_reps = 100;

   for (int rep = 1; rep <= num_reps; rep++) {
      printf("Repetition %d/%d\n", rep, num_reps);

      timeval start = {};
      gettimeofday(&start, NULL);
      gArmed1 = false;
      gArmed2 = false;

      // Spawn threads to arm the board(s)
      std::thread* t1 = new std::thread(arm_thread, &dev_handle_1, &gArmed1);
      std::thread* t2;
      
      if (num_boards == 2) {
         t2 = new std::thread(arm_thread, &dev_handle_2, &gArmed2);
      }

      // Wait for thread(s) to signal that the arm command finished
      // (or throw an error if we wait too long).
      int timeout_secs = 4;

      while (!gArmed1 || (num_boards == 2 && !gArmed2)) {
         usleep(1000);

         timeval now = {};
         gettimeofday(&now, NULL);

         if (now.tv_sec - start.tv_sec > timeout_secs) {
            throw_error("Timeout waiting for boards to be armed!!!", false);
         }
      }

      // Tidy up the threads that did the arming
      t1->join();
      delete t1;
      disarm(dev_handle_1);

      if (num_boards == 2) {
         t2->join();
         delete t2;
         disarm(dev_handle_2);
      }

      usleep(100000);
   }

   CAEN_FELib_Close(dev_handle_1);

   if (num_boards == 2) {
      CAEN_FELib_Close(dev_handle_2);
   }

   return 0;
}

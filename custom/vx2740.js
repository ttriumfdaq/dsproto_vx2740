/**
* Functions for the vslice.html page.
*/ 

var group_board_row_ids_with_override = [];
var group_board_row_ids_without_override = [];
var num_group_board_rows = 0;
var all_board_odb_classes = [];
var is_fep_mode = false;
var only_overrides = false;

// Global counter for number of modb* inputs we've created, so we can automate
// generating IDs and callback functions that refer to IDs.
var modb_idx = 0;

// Help text to show beneath parameter name.
let help_texts = {
  "LVDS quartet is input": "Configure 4 groups of 4 LVDS lines (0-3, 4-7, 8-11, 12-15).",
  "LVDS quartet mode": "SelfTriggers, Sync, IORegister, User.<br>Configure 4 groups of 4 LVDS lines (0-3, 4-7, 8-11, 12-15).<br>Sync lines are Busy/Veto/Trigger/Run.",
  "LVDS trigger mask (31-0)": "Configure each LVDS line individually.<br>Only useful if the corresponding quartet is set for output and SelfTriggers mode.",
  "LVDS trigger mask (63-32)": "Configure each LVDS line individually.<br>Only useful if the corresponding quartet is set for output and SelfTriggers mode.",
  "LVDS IO register": "Configure each LVDS line individually.<br>Only useful if the corresponding quartet is set for output and IORegister mode.",
  "User registers/LVDS output": "Configure each LVDS line individually.<br>Only useful if the corresponding quartet is set for output and User mode.",
  "Busy in source": "SIN, GPIO, LVDS, Disabled",
  "Sync out mode": "SyncIn, TestPulse, IntClk, Run, Disabled",
  "GPIO mode": "Disabled, TrgIn, P0, SIN, LVDS, ITLA, ITLB, ITLA_AND_ITLB, ITLA_OR_ITLB, EncodedClkIn, SwTrg, Run, RefClk, TestPulse, Busy, UserGPO, Fixed0, Fixed1",
  "Trigger ID mode": "TriggerCnt, EventCnt, LVDSpattern",
  "Trigger out mode": "Disabled, TRGIN, P0, SwTrg, LVDS, ITLA, ITLB, ITLA_AND_ITLB, ITLA_OR_ITLB, EncodedClkIn, Run, RefClk, TestPulse, Busy, UserTrgout, Fixed0, Fixed1, SyncIn, SIN, GPIO, LBinClk, AcceptTrg, TrgClk",
  "Veto source": "SIN, LVDS, GPIO, P0, EncodedClkIn, Disabled.<br>Multiple options allowed, separated by |",
  "Chan over thresh width (ns)": "0 means trigger signal lasts as long as the signal is over threshold.<br>Otherwise, use multiples of 8ns.",
  "Chan over thresh rising edge": "Checked means trigger is on when signal is above threshold.<br>Unchecked means trigger is on when signal is below threshold.",
  "Chan over thresh thresholds": "See also 'Use relative trig thresholds'",
  "Trigger on ch over thresh A&&B": "Only trigger if A and B fire at the same time",
  "Use external clock": "Checked means use clock from front panel connector.<br>Unchecked means use internal 62.5MHz clock.",
  "Enable clock out": "On 4-pin CLKOUT connector",
  "Ch over thresh A multiplicity": "Require N channels to be over threshold together to fire the 'A' trigger",
  "Ch over thresh B multiplicity": "Require N channels to be over threshold together to fire the 'B' trigger",
  "DC offset (pct)": "See also 'Enable DC offsets'",
  "User registers/Expert mode for trig settings": "WARNING: Expert trigger settings have odd interactions that are not intuitive. Only use if you know what you are doing!",
  "Trigger on user mode signal": "See 'Darkside self-trigger' section for threshold settings",
  "Trigger on LVDS": "This is the default CAEN logic. A quartet should be set to input and Sync mode.<br>NOT the same as the Darkside-specific 'Trigger on LVDS pair 12 signal'!",
  "Waveform length (samples)": "Max 524280 samples; multiples of 4 samples",
  "Pre-trigger (samples)": "Max 2042 samples",
  "Trigger delay (samples)": "Max 4294967295 samples",
  "DAC out mode": "Static, IPE, ChInput, MemOccupancy, ChSum, OverThrSum, Ramp, Sin5MHz, Square, User<br>IPE and MemOccupancy may not be implemented<br>User is only available in DPP firmware",
  "DAC out static level (ADC)": "Only applies if 'DAC out mode' is 'Static'. Max 16383.",
  "DAC out channel select": "Only applies if 'DAC out mode' is 'ChInput' or 'User'. 0-63.",
  //"User registers/Only read triggering channel": "If unchecked, each time a channel goes under threshold we'll read the triggering channel PLUS all channels that are in 'Readout channel mask' but NOT in 'Darkside trigger en mask'",
  "User registers/FIR filter coefficients": "-32768 to +32767 each.<br>Only first 48 coefficients used for firmware versions < 2022100500!",
  "User registers/Fixed waveform length (samples)": "Max 16380 samples; multiples of 4 samples.<br>Only applies to self-trigger if <i>dynamic readout length</i> is disabled.<br>Applies to all other trigger types.<br>This is number of samples before decimation.",
  "User registers/Pre-trigger (samples)": "Max 4095 samples.<br>This is number of samples before decimation.",
  "User registers/Post-trigger (samples)": "Max 4095 samples.<br>Only used for self-trigger, and only if <i>dynamic readout length</i> is enabled.<br>This is number of samples before decimation.",
  //"User registers/Darkside trigger en mask(31-0)": "These channels can self-trigger",
  //"User registers/Darkside trigger en mask(63-32)": "These channels can self-trigger",
  "User registers/Enable LVDS loopback": "Set LVDS output quartet 0 to be same as input quartet 1, and output quartet 2 to be same as input quartet 3.<br>Only applies if output quartets are set to User mode.",
  "User registers/Enable LVDS pair 12 trigger": "Trigger when signal seen on LVDS pair 12. Quartet 3 should be set to input and User mode.<br>NOT the same as 'Trigger on LVDS Sync signal'!<b>MUST be enabled if 'Start acq from user code` is being used.",
  "User registers/Upper 32 mirror raw of lower 32": "Raw data for upper 32 channels should be same as lower 32 channels",
  "User registers/Use raw data for DS trig": "If unchecked, will use filtered data",
  "VGA gain": "0-40dB in 0.5dB increments<br>Group 0 affects channels 0-15, group 1 affects channels 16-31 etc.",
  "Test pulse width (ns)": "Multiples of 8ns",
  "Start acq on first trigger": "Does not apply to user FW triggers - use <i>Start acq from user code</i> for that",
  "Start acq from user code": "For vertical slice this should be the ONLY 'start acq...' option enabled!",
  "User registers/Enable ADC test pulse": "ADC test pulse and CAEN test pulse are different!",
  "User registers/Enable segment stitching": "Only applies if <i>dynamic readout length</i> is enabled.",
  "User registers/Max segment length (samples)": "Only applies if <i>dynamic readout length</i> is enabled.",
  "User registers/Max stitched length (samples)": "Only applies if <i>dynamic readout length</i> and <i>enable segment stitching</i> are enabled.<br>",
  "User registers/Waveform FIFO almost full": "Will go busy when there is this free space or less left.<br>In words, max 4096. ",
  "User registers/Parameters FIFO almost full": "Will go busy when there is this free space or less left.<br>In words, max 500.",
  "User registers/VX ID": "Identifier used in communication with CDM/GDM",
  "User registers/Decimation factor": "Powers of 2 only! (1-32768)",
  "reset_cdm_link": "Reset LVDS communication link between VX and CDM",
};

let rdb_help_texts = {
  "LVDS IO register": "This reports the current LVDS state for both input quartets and output quartets.<br>The true state is reported regardless of the quartet mode.",
  "User registers/LVDS output": "This reports the LVDS state that would be output if all quartets were in output and User mode.",
  "User registers/LVDS input": "This reports the LVDS state for input quartets, regardless of the quartet mode.",
  "User registers/Num early TSM": "These are spurious TSMs that the FEPs ignore",
  "User registers/Num late TSM": "These indicate that a TSM was missed (either the GDM/CDM didn't send one, or the VX didn't write it)",
  "User registers/Busy count from CAEN layer": "We went busy because the CAEN layer was busy",
  "User registers/Busy count from param buf": "We went busy because the parameters FIFO got full",
  "User registers/Busy count from WF buf": "We went busy because the waveforms FIFO got full",
};

let display_names = {
  "Waveform length (samples)": "Scope waveform length",
  "User registers/Fixed waveform length (samples)": "Fixed waveform length",
  "Pre-trigger (samples)": "Scope pre-trigger",
  "User registers/Pre-trigger (samples)": "Pre-trigger",
  "User registers/Post-trigger (samples)": "Post-trigger",
  "User registers/Darkside trigger ToT (samples)": "Required time over threshold",
  //"User registers/Darkside trigger en mask(31-0)": "Darkside trigger enable mask (31-0)",
  //"User registers/Darkside trigger en mask(63-32)": "Darkside trigger enable mask (63-32)",
  "LVDS IO register": "CAEN LVDS IO register",
  "User registers/LVDS output": "User LVDS output",
  "User registers/LVDS input": "User LVDS input",
  "VGA gain": "VGA gain (VX2745 only)",
  "Trigger on LVDS": "Trigger on LVDS Sync signal",
  "Trigger on user mode signal": "Trigger on Darkside self-trigger",
  "Trigger on test pulse": "Trigger on CAEN test pulse",
  "User registers/Enable LVDS pair 12 trigger": "Trigger on LVDS pair 12 signal",
  "User registers/Enable LVDS TSM trigger": "Trigger TSM on LVDS pair 13 signal",
  "User registers/Enable LVDS packet trigger": "Trigger on LVDS packet",
  "User registers/Enable LVDS TSM packet trigger": "Trigger TSM on LVDS packet",
  "User registers/Trigger on falling edge": "Darkside self-trigger - trigger on falling edge",
  "User registers/Enable dynamic len for DS trig": "Darkside self-trigger - use dynamic readout length",
  "User registers/Enable ADC test pulse": "Read ADC test pulse",
  "reset_cdm_link": "Reset CDM link",
  "User registers/Num early TSM": "Num extra (premature) TSM",
  "User registers/Num late TSM": "Num missed (late) TSM",
};

/**
* Entry function that builds dynamic bits of webpage, then registers
* with midas.
*/
var vx2740_init = function() {
  mjsonrpc_db_get_values(["/Equipment", "/VX2740 defaults"]).then(function(rpc_result) {
    let equip = rpc_result.result.data[0];
    let defaults = rpc_result.result.data[1];

    let properties = calc_table_properties(equip, defaults);
    is_fep_mode = properties["is_fep_mode"];

    for (let k in properties["board_odb_classes"]) {
      all_board_odb_classes.push(properties["board_odb_classes"][k]);
    }
    
    if (properties["show_groups_table"]) {
      build_groups_table(properties);
    }

    build_force_update_buttons(properties);
    build_parameters_table(properties);
    
    mhttpd_init('VX2740 settings');
  });
};

var calc_table_properties = function(equip, defaults) {
  let properties = {};
  properties["is_fep_mode"] = false;
  properties["show_defaults_column"] = (defaults !== null)
  properties["defaults_odb_path"] = "";
  properties["show_groups_table"] = (defaults !== null)
  properties["group_odb_paths"] = {};
  properties["board_odb_paths"] = {};
  properties["board_odb_rdb"] = {};
  properties["board_odb_classes"] = {};
  properties["num_boards"] = 0;
  properties["overridden_odb_paths"] = [];
  
  if (defaults === null) {
    // Single-fe mode
    document.getElementById("group_settings").style.display = "none";
    document.getElementById("group_info").style.display = "none";
  } else {
    for (let k in equip) {
      if (k.indexOf("fep_") == 0 && k.indexOf("/") == -1) {
        // Look for settings in /Equipment/FEP_001/Settings/VX2740 instead of
        // /Equipment/VX2740_Config_Group_001/Settings.
        properties["is_fep_mode"] = true;
      }
    }

    // Group-fe mode
    if (document.getElementById("single_info") != null) {
      document.getElementById("single_info").style.display = "none";
    }
  }

  if (properties["is_fep_mode"] && document.getElementById("group_to_single_help") != null) {
    document.getElementById("group_to_single_help").style.display = "none";
  }

  if (defaults === null) {
    // Single-fe mode
    for (let k in equip) {
      if (k.indexOf("vx2740_config") == 0 && k.indexOf("group") == -1 && k.indexOf("/") == -1) {
        let proper_case = "/Equipment/" + equip[k + "/name"] + "/Settings";
        properties["board_odb_paths"][proper_case] = k.replace("vx2740_config_", "");
        properties["board_odb_rdb"][proper_case] = proper_case.replace("Settings", "Readback");
        properties["board_odb_classes"][proper_case] = k.replace("vx2740_config_", "board_");
      }
    }
  } else {
    let equip_prefix = properties["is_fep_mode"] ? "fep_0" : "vx2740_config_group_";
    properties["defaults_odb_path"] = "/VX2740 defaults"

    for (let k in equip) {
      if (k.indexOf(equip_prefix) == 0 && k.indexOf("/") == -1 && k.indexOf("monitor") == -1) {
        let settings_base = equip[k]["settings"];
        let readback_base = equip[k]["readback"];
        let suffix = "Settings";

        if (properties["is_fep_mode"]) {
          settings_base = equip[k]["vx2740"];
          suffix = "VX2740";
        }
        
        let group_proper_case = equip[k + "/name"];
        let group_odb_path = "/Equipment/" + group_proper_case + "/" + suffix;
        let group_title = k.replace(equip_prefix, "");
        properties["group_odb_paths"][group_odb_path] = group_title;

        for (let s in settings_base) {
          if (s.indexOf("board") != 0 || s.indexOf("/") != -1) {
            continue;
          }

          let board_odb_path = "/Equipment/" + group_proper_case + "/" + suffix + "/" + settings_base[s + "/name"];
          let board_title = k.replace(equip_prefix, "") + "/" + s.replace("board", "");
          properties["board_odb_paths"][board_odb_path] = board_title;
          properties["board_odb_classes"][board_odb_path] = "board_" + group_title + "_" + s.replace("board", "");

          if (properties["is_fep_mode"]) {
            properties["board_odb_rdb"][board_odb_path] = board_odb_path.replace("/VX", "/Readback/VX");
          } else {
            properties["board_odb_rdb"][board_odb_path] = board_odb_path.replace("Settings", "Readback");
          }

          let overrides = get_odb_paths_present(settings_base[s], board_odb_path);

          for (let i in overrides) {
            properties["overridden_odb_paths"].push(overrides[i]);
          }
        }
      }
    }
  }

  properties["num_groups"] = Object.keys(properties["group_odb_paths"]).length;
  properties["num_boards"] = Object.keys(properties["board_odb_paths"]).length;

  return properties;
}

var get_odb_paths_present = function(odb_obj, curr_path) {
  let retval = [];

  for (let prop in odb_obj) {
    if (prop.indexOf("/") != -1) {
      continue;
    }

    let subpath = curr_path + "/" + odb_obj[prop + "/name"];

    if (typeof(odb_obj[prop]) === 'object' && !Array.isArray(odb_obj[prop])) {
      let subpaths = get_odb_paths_present(odb_obj[prop], subpath);
      for (let i in subpaths) {
        retval.push(subpaths[i]);
      }
    } else {
      retval.push(subpath);
    }
  }

  return retval;
}

function FormatOptions() {
  this.is_boolean = false; // Show a checkbox
  this.is_array = false; // Show an array
  this.array_len = undefined; // Length of array, if is_array
  this.array_width = 4; // Number of columns if is_array
  this.show_array_edit_all = false; // Show option for changing all elements to same value.
  this.modb_format = undefined; // See https://midas.triumf.ca/MidasWiki/index.php/Custom_Page#Formatting
  this.show_ns = false; // Show box for converting between samples and ns/us/ms
  this.show_channels_0_31 = false; // Show checkboxes for converting channel 0-31 bitmasks
  this.show_channels_32_63 = false; // Show checkboxes for converting channel 32-63 bitmasks
  this.pad_right = false; // Add a bit of padding to the right
  this.changes_scope_mode = false; // Add a callback to `scope_mode_changed` if value changes
  this.changes_expert_mode = false; // Add a callback to `expert_mode_changed` if value changes
  this.changes_fw_version = false; // Add a callback to `fw_version_changed` if value changes
  this.changes_daw_mode = false; // Add a callback to `daw_mode_changed` if value changes
  this.enable_readback = true; // Read value from ODB. Only disabled for pseudo-elements that change all entries in an array at once
  this.sticky_row = false;
};

function ShowInModes() {
  this.scope = true;    // Show when running Scope mode FW
  this.open_dpp = true; // Show when running Open DPP mode FW
  this.expert = true;  // Show when expert mode enabled
  this.simple = true;  // Show when expert mode disabled
  this.fixedlen = true;  // Show when DAW mode disabled
  this.daw = true;  // Show when DAW mode enabled
};

var build_groups_table = function(properties) {
  let tot_cols = properties["num_groups"] + 1;

  let html = '<thead><tr><th class="mtableheader" colspan="' + tot_cols + '">';
  html += properties["is_fep_mode"] ? "Per-FEP" : "Per-frontend group";
  html += ' settings</th></tr></thead>';
  html += '<tbody>';
  
  if (properties["num_boards"] == 0) {
    html += '<tr><td>No group settings found</td></tr>';
  } else {
    // Header row
    html += '<tr><th>Parameter</th>';
    
    for (let k in properties["group_odb_paths"]) {
      html += '<th style="vertical-align:top">' + properties["group_odb_paths"][k] + '</th>';
    }

    let as_checkbox = new FormatOptions();
    as_checkbox.is_boolean = true;
    
    // Data rows
    html += add_group_row("Num boards (restart on change)", properties);
    html += add_group_row("Merge data using event ID", properties, as_checkbox);
    html += add_group_row("Debug data", properties, as_checkbox);
    html += add_group_row("Debug rates", properties, as_checkbox);
    html += add_group_row("Debug settings", properties, as_checkbox);
    html += add_group_row("Debug ring buffers", properties, as_checkbox);
    html += add_group_row("Multi-threaded readout", properties, as_checkbox);
    html += add_group_row("Abort if setting!=readback", properties, as_checkbox);
  }
  
  html += '</tbody>';
  document.getElementById("group_settings").innerHTML = html;
};

var build_parameters_table = function(properties) {
  let only_scope = new ShowInModes();
  only_scope.open_dpp = false;

  let only_dpp = new ShowInModes();
  only_dpp.scope = false;

  let only_dpp_expert = new ShowInModes();
  only_dpp_expert.scope = false;
  only_dpp_expert.simple = false;

  let only_dpp_simple = new ShowInModes();
  only_dpp_simple.scope = false;
  only_dpp_simple.expert = false;

  let only_dpp_fixedlen = new ShowInModes();
  only_dpp_fixedlen.scope = false;
  only_dpp_fixedlen.daw = false;

  let only_dpp_daw = new ShowInModes();
  only_dpp_daw.scope = false;
  only_dpp_daw.fixedlen = false;

  let fmt_default = new FormatOptions();

  let fmt_hex = new FormatOptions();
  fmt_hex.modb_format = "x";

  let sticky = new FormatOptions();
  sticky.sticky_row = true;

  let chan_arr = new FormatOptions();
  chan_arr.is_array = true;
  chan_arr.array_len = 64;
  chan_arr.show_array_edit_all = true;

  let chan_arr_ns = new FormatOptions();
  chan_arr_ns.is_array = true;
  chan_arr_ns.array_len = 64;
  chan_arr_ns.array_width = 1;
  chan_arr_ns.show_array_edit_all = true;
  chan_arr_ns.show_ns = true;
  
  let one_checkbox = new FormatOptions();
  one_checkbox.is_boolean = true;

  let arr_checkbox = new FormatOptions();
  arr_checkbox.is_boolean = true;
  arr_checkbox.is_array = true;
  arr_checkbox.array_len = 64;

  let bitmask_0_15 = new FormatOptions();
  bitmask_0_15.show_channels_0_15 = true;
  bitmask_0_15.modb_format = "x";
  bitmask_0_15.array_width = 1;

  let bitmask_0_31 = new FormatOptions();
  bitmask_0_31.show_channels_0_31 = true;
  bitmask_0_31.modb_format = "x";

  let bitmask_32_63 = new FormatOptions();
  bitmask_32_63.show_channels_32_63 = true;
  bitmask_32_63.modb_format = "x";

  let convert_ns = new FormatOptions();
  convert_ns.show_ns = true;

  let fmt_scope_mode = new FormatOptions();
  fmt_scope_mode.is_boolean = true;
  fmt_scope_mode.changes_scope_mode = true;

  let fmt_expert_mode = new FormatOptions();
  fmt_expert_mode.is_boolean = true;
  fmt_expert_mode.changes_expert_mode = true;

  let fmt_daw_mode = new FormatOptions();
  fmt_daw_mode.is_boolean = true;
  fmt_daw_mode.changes_daw_mode = true;

  let fmt_fw_version = new FormatOptions();
  fmt_fw_version.changes_fw_version = true;

  let fmt_lvds_input = new FormatOptions();
  fmt_lvds_input.is_boolean = true;
  fmt_lvds_input.is_array = true;
  fmt_lvds_input.array_len = 4;
  fmt_lvds_input.array_width = 1;

  let fmt_lvds_mode = new FormatOptions();
  fmt_lvds_mode.is_array = true;
  fmt_lvds_mode.array_len = 4;
  fmt_lvds_mode.array_width = 1;

  let fmt_lvds_mask = new FormatOptions();
  fmt_lvds_mask.is_array = true;
  fmt_lvds_mask.array_len = 16;
  fmt_lvds_mask.array_width = 2;
  fmt_lvds_mask.show_array_edit_all = true;
  fmt_lvds_mask.modb_format = "x";

  let fmt_vga_gain = new FormatOptions();
  fmt_vga_gain.is_array = true;
  fmt_vga_gain.array_len = 4;
  fmt_vga_gain.array_width = 1;

  let fmt_coeff = new FormatOptions();
  fmt_coeff.is_array = true;
  fmt_coeff.array_len = 64;
  fmt_coeff.array_width = 4;
  fmt_coeff.show_array_edit_all = true;

  let tot_cols = properties["num_boards"] + 1;

  if (properties["show_defaults_column"]) {
    tot_cols += 1;
  }

  let html = '<thead><tr><th class="mtableheader" colspan="' + tot_cols + '">Per-board settings</th></tr></thead>';
  html += '<tbody>';
  
  if (properties["num_boards"] == 0) {
    html += '<tr><td>No board settings found</td></tr>';
  } else {
    // Header row
    html += '<tr><th rowspan="2" style="min-width:280px">Parameter</th>';
    
    if (properties["show_defaults_column"]) {
      html += '<th rowspan="2" style="min-width:280px">Default value</th>';
    }

    let override_title = "Board settings";

    if (properties["show_defaults_column"]) {
      override_title = properties["is_fep_mode"] ? "FEP/board overrides" : "Group/board overrides";
    }

    html += '<th colspan="' + properties["num_boards"] + '">' + override_title + '</th></tr>';
    html += '<tr>';
    
    for (let k in properties["board_odb_paths"]) {
      html += '<th>' + properties["board_odb_paths"][k] + '</th>';
    }
    
    html += '</tr>';
    
    // Data rows
    html += begin_section("Main settings", properties);
    html += add_row("Hostname (restart on change)", properties, sticky);
    html += add_readback_row("Model name", properties);
    html += add_readback_row("PID", properties);
    html += add_readback_row("Firmware version", properties, fmt_fw_version);
    html += add_row("Enable", properties, one_checkbox);
    html += add_row("Read data", properties, one_checkbox), 
    html += add_row("Scope mode (restart on change)", properties, fmt_scope_mode);

    html += begin_section("Waveform readout", properties);
    html += add_row("Readout channel mask (31-0)", properties, bitmask_0_31);
    html += add_row("Readout channel mask (63-32)", properties, bitmask_32_63);
    html += add_row("User registers/Enable dynamic len for DS trig", properties, fmt_daw_mode, only_dpp, "2023022800");
    html += add_row("Waveform length (samples)", properties, convert_ns, only_scope);
    html += add_row("User registers/Fixed waveform length (samples)", properties, chan_arr_ns, only_dpp);
    html += add_row("Pre-trigger (samples)", properties, convert_ns, only_scope);
    html += add_row("User registers/Pre-trigger (samples)", properties, chan_arr_ns, only_dpp);
    html += add_row("User registers/Post-trigger (samples)", properties, chan_arr_ns, only_dpp_daw, "2023022800");
    html += add_row("User registers/Max segment length (samples)", properties, convert_ns, only_dpp_daw, "2023022800");
    html += add_row("User registers/Enable segment stitching", properties, one_checkbox, only_dpp_daw, "2023022800");
    html += add_row("User registers/Max stitched length (samples)", properties, convert_ns, only_dpp_daw, "2023022800");
    html += add_row("Read fake sinewave data", properties, one_checkbox);
    html += add_row("User registers/Enable ADC test pulse", properties, one_checkbox, only_dpp, "2023103100");
    html += add_row("User registers/ADC test pulse amplitude", properties, fmt_default, only_dpp, "2023103100");
    html += add_row("User registers/ADC test pulse period (ms)", properties, fmt_default, only_dpp, "2023103100");
    html += add_row("User registers/ADC test pulse rise time", properties, fmt_default, only_dpp, "2023103100");
    html += add_row("User registers/Apply pattern", properties, one_checkbox, only_dpp, "2024050900");
    html += add_row("User registers/Load pattern", properties, one_checkbox, only_dpp, "2024050900");
    html += add_row("User registers/Filename pattern", properties, fmt_default, only_dpp, "2024050900");
    


    html += begin_section("Global Trigger", properties);
    html += add_row("Trigger on ch over thresh A", properties, one_checkbox, only_scope);
    html += add_row("Trigger on ch over thresh B", properties, one_checkbox, only_scope);
    html += add_row("Trigger on ch over thresh A&&B", properties, one_checkbox, only_scope);
    html += add_row("Trigger on external signal", properties, one_checkbox);
    html += add_row("Trigger on software signal", properties, one_checkbox);
    html += add_row("Trigger on user mode signal", properties, one_checkbox);
    html += add_row("Trigger on test pulse", properties, one_checkbox);
    html += add_row("Trigger on LVDS", properties, one_checkbox);
    html += add_row("User registers/Enable LVDS pair 12 trigger", properties, one_checkbox, only_dpp, "2022120600");
    html += add_row("User registers/Enable LVDS packet trigger", properties, one_checkbox, only_dpp, "2025011609");
    html += add_row("User registers/Enable LVDS TSM trigger", properties, one_checkbox, only_dpp, "2023032806");
    html += add_row("User registers/Enable LVDS TSM packet trigger", properties, one_checkbox, only_dpp, "2025011609");
    html += add_row("Test pulse period (ms)", properties);
    html += add_row("Allow trigger overlap", properties, one_checkbox, only_scope);
    html += add_row("Trigger delay (samples)", properties, convert_ns, only_scope);
    html += add_row("Trigger ID mode", properties, undefined, only_scope);
    html += add_row("Trigger out mode", properties);

    html += begin_section("CAEN threshold trigger", properties, only_scope);
    html += add_row("Ch over thresh A en mask(31-0)", properties, bitmask_0_31, only_scope);
    html += add_row("Ch over thresh A en mask(63-32)", properties, bitmask_32_63, only_scope);
    html += add_row("Ch over thresh A multiplicity", properties, fmt_default, only_scope);
    html += add_row("Ch over thresh B en mask(31-0)", properties, bitmask_0_31, only_scope);
    html += add_row("Ch over thresh B en mask(63-32)", properties, bitmask_32_63, only_scope);
    html += add_row("Ch over thresh B multiplicity", properties, fmt_default, only_scope);
    html += add_row("Chan over thresh rising edge", properties, arr_checkbox, only_scope);
    html += add_row("Use relative trig thresholds", properties, one_checkbox, only_scope);
    html += add_row("Chan over thresh thresholds", properties, chan_arr, only_scope);
    html += add_row("Chan over thresh width (ns)", properties, chan_arr, only_scope);
    
    html += begin_section("Darkside self-trigger", properties, only_dpp);
    html += add_row("User registers/Expert mode for trig settings", properties, fmt_expert_mode, only_dpp);
    html += add_row("User registers/Only read triggering channel", properties, one_checkbox, only_dpp_simple);
    //html += add_row("User registers/Darkside trigger en mask(31-0)", properties, bitmask_0_31, only_dpp);
    //html += add_row("User registers/Darkside trigger en mask(63-32)", properties, bitmask_32_63, only_dpp);
    html += add_row("User registers/Use raw data for DS trig", properties, one_checkbox, only_dpp, "2023080900");
    html += add_row("User registers/Darkside trigger threshold", properties, chan_arr, only_dpp);
    html += add_row("User registers/Darkside post-trigger threshold", properties, chan_arr, only_dpp_daw, "2023022800");
    html += add_row("User registers/Darkside trigger ToT (samples)", properties, chan_arr_ns, only_dpp_daw, "2023022800");
    html += add_row("User registers/Trigger on falling edge", properties, one_checkbox, only_dpp);
    html += add_row("User registers/Trigger on threshold (31-0)", properties, bitmask_0_31, only_dpp_expert);
    html += add_row("User registers/Trigger on threshold (63-32)", properties, bitmask_32_63, only_dpp_expert);
    html += add_row("User registers/Trigger on global (31-0)", properties, bitmask_0_31, only_dpp_expert);
    html += add_row("User registers/Trigger on global (63-32)", properties, bitmask_32_63, only_dpp_expert);
    html += add_row("User registers/Trigger on internal (31-0)", properties, bitmask_0_31, only_dpp_expert);
    html += add_row("User registers/Trigger on internal (63-32)", properties, bitmask_32_63, only_dpp_expert);
    html += add_row("User registers/Trigger on external (31-0)", properties, bitmask_0_31, only_dpp_expert);
    html += add_row("User registers/Trigger on external (63-32)", properties, bitmask_32_63, only_dpp_expert);
    html += add_row("User registers/Enable decimation (31-0)", properties, bitmask_0_31, only_dpp,"2024062602");
    html += add_row("User registers/Enable decimation (63-32)", properties, bitmask_32_63, only_dpp,"2024062602");
    html += add_row("User registers/Decimation factor", properties, fmt_default, only_dpp,"2024062602");
    
    html += begin_section("Darkside filter", properties, only_dpp);
    //html += add_row("User registers/Qlong length (samples)", properties, chan_arr_ns, only_dpp);
    //html += add_row("User registers/Qshort length (samples)", properties, chan_arr_ns, only_dpp);
    html += add_row("User registers/Upper 32 mirror raw of lower 32", properties, one_checkbox, only_dpp, "2022061800");
    html += add_row("User registers/Enable FIR filter (31-0)", properties, bitmask_0_31, only_dpp);
    html += add_row("User registers/Enable FIR filter (63-32)", properties, bitmask_32_63, only_dpp);
    html += add_row("User registers/Write unfiltered data (31-0)", properties, bitmask_0_31, only_dpp, "2022112303");
    html += add_row("User registers/Write unfiltered data (63-32)", properties, bitmask_32_63, only_dpp, "2022112303");
    html += add_row("User registers/FIR filter coefficients", properties, fmt_coeff, only_dpp);

    html += begin_section("Veto", properties);
    html += add_row("Veto source", properties);
    html += add_row("Veto when source is high", properties, one_checkbox);
    html += add_row("Veto width (ns) (0=source len)", properties);

    html += begin_section("Run start", properties);
    html += add_row("Run start delay (cycles)", properties);
    html += add_row("Start acq on encoded CLKIN", properties, one_checkbox);
    html += add_row("Start acq on first trigger", properties, one_checkbox);
    html += add_row("Start acq on LVDS", properties, one_checkbox);
    html += add_row("Start acq on midas run start", properties, one_checkbox);
    html += add_row("Start acq from user code", properties, one_checkbox);
    html += add_row("Start acq on P0", properties, one_checkbox);
    html += add_row("Start acq on SIN edge", properties, one_checkbox);
    html += add_row("Start acq on SIN level", properties, one_checkbox);

    html += begin_section("Front panel", properties);
    html += add_row("Use NIM IO", properties, one_checkbox);
    html += add_row("Use external clock", properties, one_checkbox);
    html += add_row("Enable clock out", properties, one_checkbox);
    html += add_row("GPIO mode", properties);
    html += add_row("Busy in source", properties);
    html += add_row("Sync out mode", properties);
    html += add_row("LVDS quartet is input", properties, fmt_lvds_input);
    html += add_row("LVDS quartet mode", properties, fmt_lvds_mode);
    html += add_row("User registers/Enable LVDS loopback", properties, one_checkbox, only_dpp, "2022112303");
    html += add_row("User registers/Enable LVDS pair 12 trigger", properties, one_checkbox, only_dpp, "2022120600");
    html += add_row("User registers/Enable LVDS TSM trigger", properties, one_checkbox, only_dpp, "2023032806");
    html += add_row("User registers/VX ID", properties, fmt_default, only_dpp, "2024053000");
    html += add_row("LVDS IO register", properties, bitmask_0_15);
    html += add_readback_row("LVDS IO register", properties, bitmask_0_15);
    html += add_row("User registers/LVDS output", properties, bitmask_0_15, only_dpp, "2022092200");
    html += add_readback_row("User registers/LVDS output", properties, bitmask_0_15, only_dpp, "2022092200");
    html += add_readback_row("User registers/LVDS input", properties, bitmask_0_15, only_dpp, "2022092200");
    html += add_readback_row("User registers/Link status", properties, fmt_hex, only_dpp, "2024070900");
    html += add_readback_row("User registers/CDM trigger count", properties, fmt_default, only_dpp, "2024103100");
    html += add_readback_row("User registers/TSM trigger count", properties, fmt_default, only_dpp, "2024103100");
    html += add_readback_row("User registers/VX to CDM busy count", properties, fmt_default, only_dpp, "2024103100");
    html += add_readback_row("User registers/Busy count from CAEN layer", properties, fmt_default, only_dpp, "2025021200");
    html += add_readback_row("User registers/Busy count from param buf", properties, fmt_default, only_dpp, "2025021200");
    html += add_readback_row("User registers/Busy count from WF buf", properties, fmt_default, only_dpp, "2025021200");
    html += add_readback_row("User registers/CDM to VX veto count", properties, fmt_default, only_dpp, "2024103100");
    html += add_readback_row("User registers/TSM RX count", properties, fmt_default, only_dpp, "2025011609");
    html += add_readback_row("User registers/Decoded packet count", properties, fmt_default, only_dpp, "2025011609");
    html += add_readback_row("User registers/Packet error count", properties, fmt_default, only_dpp, "2025011609");
    html += add_readback_row("User registers/Hitmap status (31-0)", properties, fmt_hex, only_dpp, "2024112900");
    html += add_readback_row("User registers/Hitmap status (63-32)", properties, fmt_hex, only_dpp, "2024112900");
    html += add_readback_row("User registers/Hitmap counter", properties, fmt_default, only_dpp, "2025021200");
    html += add_readback_row("User registers/Num early TSM", properties, fmt_default, only_dpp, "2024103100");
    html += add_readback_row("User registers/Num late TSM", properties, fmt_default, only_dpp, "2024103100");
    html += add_jrpc_row("reset_cdm_link", "Reset all links", "Reset", properties, fmt_default, only_dpp, "2024070900");
    html += add_row("LVDS trigger mask (31-0)", properties, fmt_lvds_mask);
    html += add_row("LVDS trigger mask (63-32)", properties, fmt_lvds_mask);
    html += add_row("User registers/Waveform FIFO almost full", properties, fmt_default, only_dpp, "2024020502");
    html += add_row("User registers/Parameters FIFO almost full", properties, fmt_default, only_dpp, "2024020502");
    html += add_row("DAC out mode", properties);
    html += add_row("DAC out static level (ADC)", properties);
    html += add_row("DAC out channel select", properties);
    html += add_row("User registers/Show worst channel", properties, one_checkbox, only_dpp, "2024070900");
    html += add_row("User registers/Show waves FIFO level", properties, one_checkbox, only_dpp, "2024070900");
    html += add_row("User registers/Use fake hitmap", properties, one_checkbox, only_dpp, "2024112900");
    html += add_row("User registers/Fake hitmap bitmask (31-0)", properties, fmt_hex, only_dpp, "2024112900");
    html += add_row("User registers/Fake hitmap bitmask (63-32)", properties, fmt_hex, only_dpp, "2024112900");

    html += begin_section("Signal conditioning", properties);
    html += add_row("Enable DC offsets", properties, one_checkbox);
    html += add_row("DC offset (pct)", properties, chan_arr);
    html += add_row("VGA gain", properties, fmt_vga_gain);

    html += begin_section("CAEN test pulse", properties);
    html += add_row("Test pulse period (ms)", properties);
    html += add_row("Test pulse width (ns)", properties);
    html += add_row("Test pulse low level (ADC)", properties);
    html += add_row("Test pulse high level (ADC)", properties);
  }
  
  html += '</tbody>';
  document.getElementById("board_settings").innerHTML = html;
  
  // Highlight rows with differences
  for (let i in group_board_row_ids_with_override) {
    document.getElementById(group_board_row_ids_with_override[i]).className += " has_override";
  }
  for (let i in group_board_row_ids_without_override) {
    document.getElementById(group_board_row_ids_without_override[i]).className += " no_override";
  }

};

var begin_section = function(name, properties, mode_options=new ShowInModes()) {
  let row_class = row_class_for_mode_options(mode_options);
  let html = '<tr class="' + row_class + ' sticky_section"><th colspan="' + (properties["num_boards"] + 2) + '" class="mtableheader" style="font-size:smaller">';
  html += name + '<a name="' + name + '"></a>';
  html += '<div style="font-weight:normal; float:right;"><a href="#top">Back to top</a></div>';
  html += '</th></tr>';
  
  let link_text = name;

  if (mode_options.open_dpp && !mode_options.scope) {
    link_text += " (DPP only)";
  }
  if (!mode_options.open_dpp && mode_options.scope) {
    link_text += " (Scope only)";
  }
  
  document.getElementById("links").innerHTML += '<span style="display: inline-block; padding-right:30px; white-space: nowrap;"><a href="#' + name + '">' + link_text + '</a></span>';
  
  return html;
};

var row_class_for_mode_options = function(mode_options, format_options) {
  let row_class = "";

  if (mode_options.open_dpp && !mode_options.scope) {
    row_class = "dpp_row";
  } else if (!mode_options.open_dpp && mode_options.scope) {
    row_class = "scope_row";
  } else {
    row_class = "dppscope_row";
  }

  if (format_options && format_options.sticky_row) {
    row_class += " sticky_row";
  }

  return row_class;
}

/**
 * Add a row to the main table of parameters.
 * 
 * @param {string} name - Name of this parameter in the ODB
 * @param {Object} properties - 
 * @param {FormatOptions} format_options 
 * @param {ShowInModes} mode_options 
 * @param {string} min_fw_version
 * @param {string} max_fw_version
 * @return {string}
 */
var add_row = function(name, properties, format_options, mode_options=new ShowInModes(), min_fw_version="0", max_fw_version="0") {
  num_group_board_rows += 1;

  let row_id = "board_row_" + num_group_board_rows;
  let help_html = help_texts.hasOwnProperty(name) ? "<br><div style='font-size:smaller;max-width:350px;font-style:italic'>" + help_texts[name] + "</small>" : "";
  let defaults_full_path = properties["defaults_odb_path"] + "/" + name;

  let display_name = name.replace("User registers/", "");

  if (display_names.hasOwnProperty(name)) {
    display_name = display_names[name];
  }

  let row_class = row_class_for_mode_options(mode_options, format_options);

  let html = '<tr id="' + row_id  + '" class="' + row_class + '">';
  html += '<td style="vertical-align:top">' + display_name + help_html + '</td>';
  
  if (properties["show_defaults_column"]) {
    html += '<td style="vertical-align:top">';
    html += modb_html(defaults_full_path, "defaults", format_options, mode_options, min_fw_version, max_fw_version);
    html += '</td>';
  }

  let highlight = false;

  for (let board_odb_path in properties["board_odb_paths"]) {
    let full_path = board_odb_path + "/" + name;
    let extra_class = properties["board_odb_classes"][board_odb_path];

    html += '<td style="vertical-align:top">';

    if (properties["show_defaults_column"]) {
      if (properties["overridden_odb_paths"].indexOf(full_path) != -1) {
        // Setting is overridden at board-level
        html += '<button role="button" class="mbutton" onclick="del_override(\'' + full_path + '\')" style="font-size:small; float:right; padding: 0px 2px 0px 2px;">&minus;</button>';
        html += modb_html(full_path, extra_class, format_options, mode_options, min_fw_version, max_fw_version);
        highlight = true;
      } else {
        // Setting is not overridden at board-level.
        // Still want to show/hide the + button if not relevant for this board's mode.
        html += mode_html_start(extra_class, mode_options, min_fw_version, max_fw_version);
        html += '<button role="button" class="mbutton" onclick="add_override(\'' + full_path + '\', \'' + defaults_full_path + '\')" style="font-size:small; float:right; padding: 0px 2px 0px 2px;">&plus;</button>';
        html += mode_html_end(mode_options, min_fw_version, max_fw_version);
      }
    } else {
      // Default settings don't exist, so no overriding logic
      html += modb_html(full_path, extra_class, format_options, mode_options, min_fw_version, max_fw_version);
    }

    html += '</td>';
  }

  if (highlight) {
    group_board_row_ids_with_override.push(row_id);
  } else {
    group_board_row_ids_without_override.push(row_id);
  }

  return html;
};

var add_group_row = function(name, properties, format_options) {
  let html = "<tr><td>" + name + "</td>";

  for (let odb_path in properties["group_odb_paths"]) {
    html += '<td>' + modb_html(odb_path + "/" + name, "", format_options) + '</td>';// !
  }

  html += '</tr>';
  return html;
};

var add_readback_row = function(name, properties, format_options, mode_options=new ShowInModes(), min_fw_version="0", max_fw_version="0") {
  num_group_board_rows += 1;

  let row_id = "board_row_" + num_group_board_rows;
  let help_html = rdb_help_texts.hasOwnProperty(name) ? "<br><div style='font-size:smaller;max-width:350px;font-style:italic'>" + rdb_help_texts[name] + "</small>" : "";

  let display_name = name.replace("User registers/", "");

  if (display_names.hasOwnProperty(name)) {
    display_name = display_names[name];
  }

  let row_class = row_class_for_mode_options(mode_options);

  let html = '<tr id="' + row_id  + '" class="' + row_class + '">';
  html += '<td style="vertical-align:top">' + display_name + " (readback)" + help_html + '</td>';
  
  if (properties["show_defaults_column"]) {
    html += '<td style="vertical-align:top"></td>';
  }

  for (let board_odb_path in properties["board_odb_paths"]) {
    let full_path = properties["board_odb_rdb"][board_odb_path] + "/" + name;

    let extra_class = properties["board_odb_classes"][board_odb_path];

    html += '<td style="vertical-align:top">';
    html += modb_html(full_path, extra_class, format_options, mode_options, min_fw_version, max_fw_version, true);
    html += '</td>';

    // Don't highlight readback rows
    group_board_row_ids_without_override.push(row_id);
  }

  return html;
};

/**
 * Add a row to the main table of parameters.
 * 
 * @param {string} name - Name of this JRPC command
 * @param {string} label_all - 
 * @param {string} label_single - 
 * @param {Object} properties - 
 * @param {FormatOptions} format_options 
 * @param {ShowInModes} mode_options 
 * @param {string} min_fw_version
 * @param {string} max_fw_version
 * @return {string}
 */
var add_jrpc_row = function(name, label_all, label_single, properties, format_options, mode_options=new ShowInModes(), min_fw_version="0", max_fw_version="0") {
  num_group_board_rows += 1;

  let row_id = "board_row_" + num_group_board_rows;
  let help_html = help_texts.hasOwnProperty(name) ? "<br><div style='font-size:smaller;max-width:350px;font-style:italic'>" + help_texts[name] + "</small>" : "";
  let display_name = name;
  let button_class = "jrpc_button_" + num_group_board_rows;

  if (display_names.hasOwnProperty(name)) {
    display_name = display_names[name];
  }

  let row_class = row_class_for_mode_options(mode_options, format_options);

  let html = '<tr id="' + row_id  + '" class="' + row_class + '">';
  html += '<td style="vertical-align:top">' + display_name + help_html + '</td>';
  
  if (properties["show_defaults_column"]) {
    html += '<td style="vertical-align:top">';
    html += '<button onclick="click_all_buttons_with_class(\'' + button_class + '\')">' + label_all + '</button>';
    html += '</td>';
  }

  for (let board_odb_path in properties["board_odb_paths"]) {
    let extra_class = properties["board_odb_classes"][board_odb_path];
    let group_board = properties["board_odb_paths"][board_odb_path].split("/");
    let group = group_board[0];
    let board = group_board[1];
    let button_id = "button_" + num_group_board_rows + "_" + group + "_" + board;
    html += '<td style="vertical-align:top">';
    html += mode_html_start(extra_class, mode_options, min_fw_version, max_fw_version);
    html += '<button id="' + button_id + '" class="' + button_class + '" onclick="run_jrpc(\'' + name + '\', ' + group + ', \'' + board + '\', \'' + button_id + '\', \'Reset done!\')">' + label_single + '</button>';
    html += mode_html_end(mode_options, min_fw_version, max_fw_version);

    html += '</td>';
  }

  return html;
};

/**
 * Zero-pad an integer. E.g. `pad_zero(1, 3)` returns "001".
 * 
 * @param {int} num - The number
 * @param {int} len - The string length to create
 * @return {string} - The 0-padded representation
 */
var pad_zero = function(num, len) {
  let zeros = "";
  for (let i = 0; i < len; i++) {
    zeros += "0";
  }
  return (zeros + num).slice(-1 * len);
};

/**
 * Create HTML for wrapping content with a div that can be shown/hidden based
 * on whether a board is in scope/DPP mode and/or expert mode.
 * 
 * @param {string} extra_class 
 * @param {ShowInModes} mode_options 
 * @param {string} min_fw_version
 * @param {string} max_fw_version
 * @returns {string} - HTML
 */
var mode_html_start = function(extra_class, mode_options, min_fw_version, max_fw_version) {
  let retval = "";

  if (mode_options.scope && !mode_options.open_dpp) {
    retval += '<div style="display:none; font-style:italic; font-size:smaller; width:100%;" class="dpp ' + extra_class + '">Scope FW only</div><div style="display:inline-block; width:100%;" class="scope ' + extra_class + '">';
  } else if (!mode_options.scope && mode_options.open_dpp) {
    retval += '<div style="display:none; font-style:italic; font-size:smaller; width:100%;" class="scope ' + extra_class + '">DPP FW only</div><div style="display:inline-block; width:100%;" class="dpp ' + extra_class + '">';
  }

  if (mode_options.expert && !mode_options.simple) {
    retval += '<div style="display:none; font-style:italic; font-size:smaller; width:100%;" class="simple ' + extra_class + '">Expert mode only</div><div style="display:inline-block; width:100%;" class="expert ' + extra_class + '">';
  } else if (!mode_options.expert && mode_options.simple) {
    retval += '<div style="display:none; font-style:italic; font-size:smaller; width:100%;" class="expert ' + extra_class + '">Non-expert mode only</div><div style="display:inline-block; width:100%;" class="simple ' + extra_class + '">';
  }

  if (mode_options.daw && !mode_options.fixedlen) {
    retval += '<div style="display:none; font-style:italic; font-size:smaller; width:100%;" class="fixedlen ' + extra_class + '">Dynamic readout length mode only</div><div style="display:inline-block; width:100%;" class="daw ' + extra_class + '">';
  } else if (!mode_options.daw && mode_options.fixedlen) {
    retval += '<div style="display:none; font-style:italic; font-size:smaller; width:100%;" class="daw ' + extra_class + '">Fixed-length readout mode only</div><div style="display:inline-block; width:100%;" class="fixedlen ' + extra_class + '">';
  }

  if (min_fw_version != "0" || max_fw_version != "0") {
    let data = "";
    if (min_fw_version != "0") {
      data += ' data-min-fw="' + min_fw_version + '" ';
    }
    if (max_fw_version != "0") {
      data += ' data-max-fw="' + max_fw_version + '" ';
    }
    retval += '<div style="display:none; font-style:italic; font-size:smaller; width:100%;" class="fw_out_of_range ' + extra_class + '" ' + data + '>Firmware versions >= ' + min_fw_version + ' only</div><div style="display:inline-block; width:100%;" class="fw_okay ' + extra_class + '" ' + data + '">';
  }

  return retval;
}

/**
 * Create HTML for ending any div that was created with `mode_html_start()`.
 * 
 * @param {ShowInModes} mode_options 
 * @returns {string} - HTML
 */
var mode_html_end = function(mode_options, min_fw_version, max_fw_version) {
  let retval = "";

  if (mode_options.scope && !mode_options.open_dpp) {
    retval += '</div>';
  } else if (!mode_options.scope && mode_options.open_dpp) {
    retval += '</div>';
  }

  if (mode_options.expert && !mode_options.simple) {
    retval += '</div>';
  } else if (!mode_options.expert && mode_options.simple) {
    retval += '</div>';
  }

  if (mode_options.daw && !mode_options.fixedlen) {
    retval += '</div>';
  } else if (!mode_options.daw && mode_options.fixedlen) {
    retval += '</div>';
  }

  if (min_fw_version != "0" || max_fw_version != "0") {
    retval += '</div>';
  }

  return retval;
}

/**
* Create HTML for displaying a checkbox or field for editing an ODB value.
*
* @protected
* @param {string} odb_path - ODB location
* @param {string} extra_class
* @param {FormatOptions} format_options - 
* @param {ShowInModes} mode_options - 
* @param {string} min_fw_version -
* @param {string} max_fw_version -
* @param {boolean} readonly - 
* @return {string} - HTML
*/
var modb_html = function(odb_path, extra_class, format_options=new FormatOptions(), mode_options=new ShowInModes(), min_fw_version="0", max_fw_version="0", readonly=false) {
  // Outer div for showing hiding if in scope mode.
  let html = mode_html_start(extra_class, mode_options, min_fw_version, max_fw_version);
  let editable_str = readonly ? '' : ' data-odb-editable="1" ';

  if (format_options.is_array) {
    // Format for element within array
    let elem_options = new FormatOptions();
    Object.assign(elem_options, format_options);
    elem_options.is_array = false;
    elem_options.pad_right = true;

    if (format_options.show_array_edit_all) {
      let edit_all_options = new FormatOptions();
      Object.assign(edit_all_options, format_options);
      edit_all_options.is_array = false;
      edit_all_options.pad_right = false;
      edit_all_options.enable_readback = false;

      html += 'Set all to ' + modb_html(odb_path + '[*]', "", edit_all_options) + "<br>";
    }

    for (let i = 0; i < format_options.array_len; i++) {
      let idx = '[' + pad_zero(i, 2) + ']';
      html += idx + " ";
      html += modb_html(odb_path + idx, "", elem_options);
      if (i % format_options.array_width == format_options.array_width-1) {
        html += '<br>';
      }
    }
  } else if (format_options.is_boolean) {
    let classes = "modbcheckbox";
    html += '<input type="checkbox" data-odb-path="' + odb_path + '" ' + editable_str;
    
    if (format_options.pad_right) {
      html += ' style="margin-right:15px" ';
    }

    if (format_options.changes_scope_mode) {
      html += ' onchange="scope_mode_changed(\'' + extra_class + '\')" onload="onchange()" ';
      html += ' id="scope_mode_cb_' + extra_class + '"';
      classes += ' scope_mode_cb';
    }

    if (format_options.changes_expert_mode) {
      html += ' onchange="expert_mode_changed(\'' + extra_class + '\')" onload="onchange()" ';
      html += ' id="expert_mode_cb_' + extra_class + '"';
      classes += ' expert_mode_cb';
    }

    if (format_options.changes_daw_mode) {
      html += ' onchange="daw_mode_changed(\'' + extra_class + '\')" onload="onchange()" ';
      html += ' id="daw_mode_cb_' + extra_class + '"';
      classes += ' daw_mode_cb';
    }

    html += ' class="' + classes + '" ';

    html += '/>';
  } else {
    modb_idx += 1;
    let fmt_str = "";
    if (format_options.modb_format !== undefined) {
      fmt_str = ' data-format="' + format_options.modb_format + '"';
    }

    let onchange = "";
    let div_id = 'modb_' + modb_idx;
    let div_class = "";

    if (format_options.show_ns) {
      onchange = ' onchange="convert_to_us(this.value, ' + modb_idx + ')" onload="onchange()" ';
    } else if (format_options.show_channels_0_15) {
      onchange = ' onchange="convert_to_checkboxes(this.value, ' + modb_idx + ', 16)" onload="onchange()" ';
    } else if (format_options.show_channels_0_31) {
      onchange = ' onchange="convert_to_checkboxes(this.value, ' + modb_idx + ', 32)" onload="onchange()" ';
    } else if (format_options.show_channels_32_63) {
      onchange = ' onchange="convert_to_checkboxes(this.value, ' + modb_idx + ', 32)" onload="onchange()" ';
    } else if (format_options.changes_fw_version) {
      onchange = ' onchange="fw_version_changed(this.value, \'' + extra_class + '\')" onload="onchange()" ';
      div_class = "fw_version";
      div_id = "fw_version_" + extra_class;
    }

    if (format_options.pad_right) {
      html += '<span style="margin-right:15px">';
    }

    if (format_options.enable_readback) {
      html += '<span id="' + div_id + '" class="modbvalue ' + div_class + '" data-odb-path="' + odb_path + '" ' + editable_str + fmt_str + onchange + '></span>';
    } else {
      html += '<input id="' + div_id + '" class="odb_edit_all ' + div_class + '" data-odb-path="' + odb_path + '" ' + fmt_str + '></span>';
    }

    if (format_options.show_ns) {
      if (format_options.enable_readback) {
        html += ' samples (<input id="us_' + modb_idx + '" style="max-width:50px" onblur="convert_from_us(' + modb_idx + ')"' + (readonly ? "disabled" : "") + '>&mu;s)';
      } else {
        html += ' samples ';
      }
    } else if (format_options.show_channels_0_15 || format_options.show_channels_0_31 || format_options.show_channels_32_63) {
      let offset = format_options.show_channels_32_63 ? 32 : 0;
      let len = format_options.show_channels_0_15 ? 16 : 32;
      html += '<br>';
      for (let chan = 0; chan < len; chan++) {
        html += '[' + pad_zero(chan + offset ,2) + '] <input style="margin-right:15px;" type="checkbox" id="cb_' + modb_idx + '_' + chan + '" onclick="convert_from_checkboxes(' + modb_idx + ', ' + len + ')" ' + (readonly ? "disabled" : "") + '>';
        if (chan % format_options.array_width == format_options.array_width - 1) { html += '<br>';}
      }
    }

    if (!format_options.enable_readback) {
      html += '<button class="mbutton" role="button" style="font-size:smaller" onclick="submit_change_all(' + modb_idx + ')">Go</button>';
    }

    if (format_options.pad_right) {
      html += '</span>';
    }
  }

  html += mode_html_end(mode_options, min_fw_version, max_fw_version);

  return html;    
};

var submit_change_all = function(modb_idx) {
  let elem = document.getElementById('modb_' + modb_idx);
  let value = elem.value;
  let odb_path = elem.dataset.odbPath;

  mjsonrpc_db_set_value(odb_path, value).then(function(rpc) {
  }).catch(function(error) {
    mjsonrpc_error_alert(error);
  });
};

/**
 * Convert the value in an input file (specified in us) to a number of samples,
 * then set that value in the ODB.
 * 
 * @param {int} modb_idx - Suffix of element IDs for input and modbvalue (modb_X and us_X).
 */
var convert_from_us = function(modb_idx) {
  let val_us = parseFloat(document.getElementById("us_" + modb_idx).value);
  let val_samples = Math.floor(val_us * 125);
  let odb_path = document.getElementById("modb_" + modb_idx).dataset.odbPath;

  mjsonrpc_db_set_value(odb_path, val_samples).then(function(rpc) {
  }).catch(function(error) {
    mjsonrpc_error_alert(error);
  });
};

/**
 * Convert an ODB value (stored in samples) to a time in us shown in an input field..
 * 
 * @param {int} val_samples - New duration in samples (8ns per sample)
 * @param {int} modb_idx - Suffix of element IDs for input and modbvalue (modb_X and us_X).
 */
var convert_to_us = function(val_samples, modb_idx) {
  let val_us = val_samples/125;
  document.getElementById("us_" + modb_idx).value = val_us;
};

/**
 * Convert the state of 32 checkboxes to a 32-bit bitmask, and store that value in the ODB.
 * 
 * @param {int} modb_idx - Suffix of element IDs for checkboxes and modbvalue (modb_X and cb_X_Y).
 * @param {int=} len - Number of checkboxes.
 */
var convert_from_checkboxes = function(modb_idx, len) {
  if (len === undefined) {
    len = 32;
  }
  let mask = 0;
  for (let i = 0; i < len; i++) {
    let el = document.getElementById("cb_" + modb_idx + "_" + i);
    if (el.checked) {
      mask |= 1 << i;
    }
  }

  // Avoid negative hex values in JS.
  mask = mask >>> 0;

  // Set new value in ODB
  let odb_path = document.getElementById("modb_" + modb_idx).dataset.odbPath;

  mjsonrpc_db_set_value(odb_path, mask).then(function(rpc) {
  }).catch(function(error) {
    mjsonrpc_error_alert(error);
  });
};

/**
 * Convert an ODB value (bitmask) into checked state of 32 checkboxes.
 * 
 * @param {int} mask - New bitmask
 * @param {int} modb_idx - Suffix of element IDs for checkboxes and modbvalue (modb_X and cb_X_Y).
 * @param {int=} len - Number of checkboxes.
 */
var convert_to_checkboxes = function(mask, modb_idx, len) {
  if (len === undefined) {
    len = 32;
  }
  
  for (let i = 0; i < len; i++) {
    document.getElementById("cb_" + modb_idx + "_" + i).checked = (mask & (1<<i));
  }
};

var scope_mode_changed = function(changed_board_class) {
  // Figure out which boards are in scope/dpp mode
  let all_scope_cbs = document.getElementsByClassName('scope_mode_cb');
  let scope_default = true;
  let any_scope = false;
  let any_dpp = false;
  let scope_boards = {};

  for (let i = 0; i < all_scope_cbs.length; i++) {
    let el = all_scope_cbs[i];
    let is_scope = el.checked;

    any_scope = any_scope | is_scope;
    any_dpp = any_dpp | !is_scope;

    let board_class = el.id.replace("scope_mode_cb_", "");

    if (board_class == "defaults") {
      scope_default = is_scope;
    } else {
      scope_boards[board_class] = is_scope;
    }
  }

  // Determine scope/dpp for each board based on per-board and default settings
  for (let i in all_board_odb_classes) {
    let cl = all_board_odb_classes[i];
    if (!scope_boards.hasOwnProperty(cl)) {
      scope_boards[cl] = scope_default;
    }
  }

  // Show/hide rows based on whether any boards are in scope/dpp
  let dpp_elems = document.getElementsByClassName("dpp_row");
  for (let i = 0; i < dpp_elems.length; i++) {
    let hide = (!any_dpp) || (only_overrides && dpp_elems[i].className.indexOf("no_override") != -1);
    dpp_elems[i].style.display = hide ? "none" : "table-row";
  }

  let scope_elems = document.getElementsByClassName("scope_row");
  for (let i = 0; i < scope_elems.length; i++) {
    let hide = (!any_scope) || (only_overrides && scope_elems[i].className.indexOf("no_override") != -1);
    scope_elems[i].style.display = hide ? "none" : "table-row";
  }

  let dppscope_elems = document.getElementsByClassName("dppscope_row");
  for (let i = 0; i < dppscope_elems.length; i++) {
    let hide = (only_overrides && dppscope_elems[i].className.indexOf("no_override") != -1);
    dppscope_elems[i].style.display = hide ? "none" : "table-row";
  }

  // Show/hide defaults based on whether any boards are in scope/dpp
  if (scope_default !== undefined) {
    let elems = document.getElementsByClassName("defaults");

    for (let i = 0; i < elems.length; i++) {
      let el = elems[i];

      if (el.className.indexOf("scope") != -1) {
        el.style.display = any_scope ? 'inline-block' : 'none';
      } 
      if (el.className.indexOf("dpp") != -1) {
        el.style.display = any_dpp ? 'inline-block' : 'none';
      } 
    }
  }

  // Show/hide per-board settings based on whether that board is in scope/dpp
  for (let board_class in scope_boards) {
    let is_scope = scope_boards[board_class];
    let elems = document.getElementsByClassName(board_class);

    for (let i = 0; i < elems.length; i++) {
      let el = elems[i];

      if (is_scope) {
        if (el.className.indexOf("scope") != -1) {
          el.style.display = 'inline-block';
        } 
        if (el.className.indexOf("dpp") != -1) {
          el.style.display = 'none';
        } 
      } else {
        if (el.className.indexOf("scope") != -1) {
          el.style.display = 'none';
        } 
        if (el.className.indexOf("dpp") != -1) {
          el.style.display = 'inline-block';
        } 
      }
    }
  }
};

var expert_mode_changed = function(changed_board_class) {
  let new_is_expert = document.getElementById("expert_mode_cb_" + changed_board_class).checked;

  // Figure out which boards are in expert/non-expert mode
  let all_expert_cbs = document.getElementsByClassName('expert_mode_cb');
  let expert_default = true;
  let any_expert = false;
  let any_simple = false;
  let expert_boards = {};

  for (let i = 0; i < all_expert_cbs.length; i++) {
    let el = all_expert_cbs[i];
    let is_expert = el.checked;

    any_expert = any_expert | is_expert;
    any_simple = any_simple | !is_expert;

    let board_class = el.id.replace("expert_mode_cb_", "");

    if (board_class == "defaults") {
      expert_default = is_expert;
    } else {
      expert_boards[board_class] = is_expert;
    }
  }

  if (expert_default === undefined) {
    // Single-FE mode. Just change the board that changed.
    expert_boards = {changed_board_class: new_is_expert};
  } else {
    // Determine expert/simple for each board based on per-board and default settings
    for (let i in all_board_odb_classes) {
      let cl = all_board_odb_classes[i];
      if (!expert_boards.hasOwnProperty(cl)) {
        expert_boards[cl] = expert_default;
      }
    }
  }

  if (expert_default !== undefined) {
    // Show/hide defaults based on whether any boards are in expert/simple
    let elems = document.getElementsByClassName("defaults");

    for (let i = 0; i < elems.length; i++) {
      let el = elems[i];

      if (el.className.indexOf("expert") != -1) {
        el.style.display = any_expert ? 'inline-block' : 'none';
      }
      if (el.className.indexOf("simple") != -1) {
        el.style.display = any_simple ? 'inline-block' : 'none';
      }
    }
  }

  // Show/hide per-board settings based on whether that board is in scope/dpp
  for (let board_class in expert_boards) {
    let is_expert = expert_boards[board_class];
    let elems = document.getElementsByClassName(board_class);

    for (let i = 0; i < elems.length; i++) {
      let el = elems[i];

      if (is_expert) {
        if (el.className.indexOf("expert") != -1) {
          el.style.display = 'inline-block';
        } 
        if (el.className.indexOf("simple") != -1) {
          el.style.display = 'none';
        } 
      } else {
        if (el.className.indexOf("expert") != -1) {
          el.style.display = 'none';
        } 
        if (el.className.indexOf("simple") != -1) {
          el.style.display = 'inline-block';
        } 
      }
    }
  }
};

var daw_mode_changed = function(changed_board_class) {
  let new_is_daw = document.getElementById("daw_mode_cb_" + changed_board_class).checked;

  // Figure out which boards are in DAW/fixed-length mode
  let all_daw_cbs = document.getElementsByClassName('daw_mode_cb');
  let daw_default = true;
  let any_daw = false;
  let any_fixedlen = false;
  let daw_boards = {};

  for (let i = 0; i < all_daw_cbs.length; i++) {
    let el = all_daw_cbs[i];
    let is_daw = el.checked;

    any_daw = any_daw | is_daw;
    any_fixedlen = any_fixedlen | !is_daw;

    let board_class = el.id.replace("daw_mode_cb_", "");

    if (board_class == "defaults") {
      daw_default = is_daw;
    } else {
      daw_boards[board_class] = is_daw;
    }
  }

  if (daw_default === undefined) {
    // Single-FE mode. Just change the board that changed.
    daw_boards = {changed_board_class: new_is_daw};
  } else {
    // Determine DAW/fixed-length for each board based on per-board and default settings
    for (let i in all_board_odb_classes) {
      let cl = all_board_odb_classes[i];
      if (!daw_boards.hasOwnProperty(cl)) {
        daw_boards[cl] = daw_default;
      }
    }
  }

  if (daw_default !== undefined) {
    // Show/hide defaults based on whether any boards are in DAW/fixed-length
    let elems = document.getElementsByClassName("defaults");

    for (let i = 0; i < elems.length; i++) {
      let el = elems[i];

      if (el.className.indexOf("daw") != -1) {
        el.style.display = any_daw ? 'inline-block' : 'none';
      }
      if (el.className.indexOf("fixedlen") != -1) {
        el.style.display = any_fixedlen ? 'inline-block' : 'none';
      }
    }
  }

  // Show/hide per-board settings based on whether that board is in scope/dpp
  for (let board_class in daw_boards) {
    let is_daw = daw_boards[board_class];
    let elems = document.getElementsByClassName(board_class);

    for (let i = 0; i < elems.length; i++) {
      let el = elems[i];

      if (is_daw) {
        if (el.className.indexOf("daw") != -1) {
          el.style.display = 'inline-block';
        } 
        if (el.className.indexOf("fixedlen") != -1) {
          el.style.display = 'none';
        } 
      } else {
        if (el.className.indexOf("daw") != -1) {
          el.style.display = 'none';
        } 
        if (el.className.indexOf("fixedlen") != -1) {
          el.style.display = 'inline-block';
        } 
      }
    }
  }
};

var fw_version_changed = function(new_fw_version, changed_board_class) {
  // Figure out which boards are in expert/non-expert mode
  let all_fw_versions = document.getElementsByClassName('fw_version');
  let all_fw_same = true;

  for (let i = 0; i < all_fw_versions.length; i++) {
    let el = all_fw_versions[i];
    let this_fw_ver = el.innerHTML;
    console.log("/" + this_fw_ver + "/" + new_fw_version + "/");

    if (this_fw_ver != new_fw_version) {
      all_fw_same = false;
      break;
    }
  }

  // Show/hide defaults only if all boards have same FW
  let elems = document.getElementsByClassName("defaults");

  for (let i = 0; i < elems.length; i++) {
    let el = elems[i];
    let fw_okay = true;

    if (el.dataset.hasOwnProperty("minFw") && new_fw_version < el.dataset.minFw) {
      fw_okay = false;
    }

    if (el.dataset.hasOwnProperty("maxFw") && new_fw_version > el.dataset.maxFw) {
      fw_okay = false;
    }

    if (!all_fw_same) {
      el.style.display = 'inline-block';
    } else {
      if (el.className.indexOf("fw_out_of_range") != -1) {
        el.style.display = !fw_okay ? 'inline-block' : 'none';
      }
      if (el.className.indexOf("fw_okay") != -1) {
        el.style.display = fw_okay  ? 'inline-block' : 'none';
      }
    }
  }

  // Show/hide per-board settings based on FW version
  elems = document.getElementsByClassName(changed_board_class);

  for (let i = 0; i < elems.length; i++) {
    let el = elems[i];
    let fw_okay = true;

    if (el.dataset.hasOwnProperty("minFw") && new_fw_version < el.dataset.minFw) {
      fw_okay = false;
    }

    if (el.dataset.hasOwnProperty("maxFw") && new_fw_version > el.dataset.maxFw) {
      fw_okay = false;
    }

    if (el.className.indexOf("fw_out_of_range") != -1) {
      el.style.display = !fw_okay ? 'inline-block' : 'none';
    }
    if (el.className.indexOf("fw_okay") != -1) {
      el.style.display = fw_okay  ? 'inline-block' : 'none';
    }
  }
};

var build_force_update_buttons = function(properties) {
  let html = "";

  for (let k in properties["group_odb_paths"]) {
    let idx = properties["group_odb_paths"][k];
    html += '<button role="button" class="mbutton" id="force_update_' + idx + '" onclick="force_write(\'' + idx + '\')">Apply settings to group ' + idx + ' now</button>';
  }

  document.getElementById("force_update_buttons").innerHTML = html;
};

var build_params_for_jrpc = function(cmd, group_idx, args="") {
  let params = Object();
  let zero_pad;

  if (is_fep_mode) {
    zero_pad = ("000" + group_idx).slice(-3);
    params.client_name = "FEP_" + zero_pad;
  } else {
    zero_pad = ("00" + group_idx).slice(-2);
    params.client_name = "VX2740_Group_" + zero_pad;
  }
  
  params.cmd = cmd;
  params.args = args;

  return params;
}

var click_all_buttons_with_class = function(button_class) {
  let elems = document.getElementsByClassName(button_class);

  for (let e = 0; e < elems.length; e++) {
    elems[e].click();
  }
}

var run_jrpc = function(cmd, group_idx, args, button_id, ok_text, silent_failure=true) {
  let params = build_params_for_jrpc(cmd, group_idx, args);
  document.getElementById(button_id).disabled = true;

  mjsonrpc_call("jrpc", params).then(function(rpc) {
    throw_if_jrpc_failed(rpc, true);
    let curr_text = document.getElementById(button_id).innerText;
    document.getElementById(button_id).disabled = false;
    document.getElementById(button_id).innerText = ok_text;
    setTimeout(function(){document.getElementById(button_id).innerText = curr_text;}, 1000);
  }).catch(function(error) {
    let curr_text = document.getElementById(button_id).innerText;
    document.getElementById(button_id).disabled = false;
    document.getElementById(button_id).innerText = error;
    setTimeout(function(){document.getElementById(button_id).innerText = curr_text;}, 1000);

    if (!silent_failure) {
      mjsonrpc_error_alert(error);
    }
  });
}

var throw_if_jrpc_failed = function(rpc, terse=false) {
  if (rpc.result.status == 103) {
    throw(terse ? "FE not running!" : "Failed to run update! Client " + rpc.params.client_name + " isn't running!");
  }
  if (rpc.result.status != 1) {
    if (terse) {
      throw("Failed: " + rpc.result.status);
    } else if (rpc.result.reply === "" || rpc.result.reply === undefined) {
      throw("Failed to run update! Response code was " + rpc.result.status + ". See Messages page for more.");
    } else {
      throw("Failed to run update! Response code was " + rpc.result.status + ": " + rpc.result.reply);
    }
  }
}

// Special version of run_jrpc that disables/enables a button while action
// is performed.
var force_write = function(group_idx) {
  let params = build_params_for_jrpc("force_write", group_idx);

  document.getElementById("force_update_" + group_idx).disabled = true;
  let orig_text = document.getElementById("force_update_" + group_idx).innerText;
  document.getElementById("force_update_" + group_idx).innerText = "Applying settings...";

  mjsonrpc_call("jrpc", params).then(function(rpc) {
    throw_if_jrpc_failed(rpc);
    document.getElementById("force_update_" + group_idx).disabled = false;
    document.getElementById("force_update_" + group_idx).innerText = "Settings applied!";
    setTimeout(function(){document.getElementById("force_update_" + group_idx).innerText = "Apply settings to group " + group_idx + " now";}, 1000);
  }).catch(function(error) {
    document.getElementById("force_update_" + group_idx).disabled = false;
    document.getElementById("force_update_" + group_idx).innerText = "Failed to apply settings!";
    setTimeout(function(){document.getElementById("force_update_" + group_idx).innerText = "Apply settings to group " + group_idx + " now";}, 1000);
    mjsonrpc_error_alert(error);
  });
};

var add_override = function(new_odb_path, copy_odb_path) {
  // Both db_key and db_get_values use the "paths" parameter
  let params = Object();
  params.paths = [copy_odb_path];
  let key_req = mjsonrpc_make_request("db_key", params);
  let val_req = mjsonrpc_make_request("db_get_values", params);

  mjsonrpc_send_request([key_req, val_req]).then(function(rpc) {
    let key = rpc[0].result.keys[0];
    let val = rpc[1].result.data[0];
    
    let new_odb = Object();
    new_odb.path = new_odb_path;
    new_odb.type = key.type;
    new_odb.array_length = key.num_values;
    
    if (new_odb.type == 12) {
      // String types
      new_odb.string_length = key.item_size;
    }
    
    mjsonrpc_db_create([new_odb]).then(function(rpc) {
      mjsonrpc_db_set_value(new_odb_path, val).then(function(rpc) {
        location.reload();
      });
    });
  });

};

var del_override = function(odb_path) {
  mjsonrpc_db_delete([odb_path]).then(function(rpc) {
    location.reload();
  });
};

var toggle_only_overrides = function() {
  only_overrides = !only_overrides;

  if (only_overrides) {
    document.getElementById("only_overrides").innerText = "Show all rows";
  } else {
    document.getElementById("only_overrides").innerText = "Show only rows with per-board overrides";
  }

  scope_mode_changed();
}
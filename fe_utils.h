#ifndef FE_UTILS_H
#define FE_UTILS_H

#include <string>

namespace fe_utils {
   /**
    * Like printf, but automatically prepend a timestamp.
    */ 
   void ts_printf(const char *format, ...);

   /**
    * Return a string like "1.23GiB" for human-readable data sizes.
    */ 
   std::string format_bytes(int num_bytes);

   /**
    * Whether board_version is before cmp_version;
    */
   bool is_fw_version_before(std::string board_version, std::string cmp_version);

   /**
    * Current UNIX timestamp in ms.
    */
   uint64_t current_time_ms();
};

#endif
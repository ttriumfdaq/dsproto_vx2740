// Dump VX data from a MIDAS file 
// Usage ./dump_vx2740_data <filename> 
// Example: ./dump_vx2740_data "/home/dsdaq/madry/data/run002387_tsp001_subrun0000.mid"

#include <iostream>
#include <vector>
#include <map>
#include <cinttypes>
#include <cstring>
#include "midasio.h"

typedef struct VXMidasEvent {
  int fe_idx;
  int bd_idx;
  uint32_t midas_serial_number;
  uint32_t vx_event_counter;
  uint16_t flags;
  uint8_t  overlap;
  uint64_t trigger_time_ticks;
  double trigger_time_secs;
  std::map<int, std::vector<uint16_t>> waveforms;

  bool decode(TMEvent* event, TMBank& bank) {
    uint64_t* data = (uint64_t*)event->GetBankData(&bank);
    uint16_t format = (data[0] >> 56) & 0xFF;
      
    if (format != 0x10) {
      // Not a regular data event
      return false;
    }

    fe_idx = event->trigger_mask;
    bd_idx = -1;
    sscanf(bank.name.c_str(), "D%d", &bd_idx);
    midas_serial_number = event->serial_number;
    vx_event_counter = (data[0] >> 32) & 0xFFFFFF;
    uint32_t size_64bit_words = data[0] & 0xFFFFFFFF;
    flags = data[1] >> 52;
    overlap = (data[1] >> 48) & 0xF;
    trigger_time_ticks = data[1] & 0xFFFFFFFFFFFF;
    trigger_time_secs = trigger_time_ticks / 1.25e8;
          
    int num_header_words = 3;
    uint64_t chan_enable_mask = data[2];

    waveforms.clear();

    for (int chan = 0; chan < 64; chan++) {
      if (chan_enable_mask & (((uint64_t)1)<<chan)) {
        waveforms[chan] = std::vector<uint16_t>();
      }
    }

    uint32_t num_chans = waveforms.size();
    uint32_t num_words_per_chan = (size_64bit_words - num_header_words) / num_chans;
    uint32_t num_samples_per_chan = num_words_per_chan * 4;

    for (auto& wf : waveforms) {
      wf.second.resize(num_samples_per_chan);
    }

    if (num_chans == 1) {
      // One channel - can just copy data directly
      memcpy(waveforms.begin()->second.data(), &(data[num_header_words]), sizeof(uint64_t) * num_words_per_chan);
    } else {
      // Multiple channels - data is interleaved (4 samples per channel)
      for (int wd = 0; wd < num_words_per_chan; wd++) {
        int idx_in_data = num_header_words + wd * num_chans;

        for (auto& wf : waveforms) {
          int idx_in_wf = wd * 4;
          uint64_t word = data[idx_in_data];
          wf.second[idx_in_wf + 0] = (word & 0xFFFF);
          wf.second[idx_in_wf + 1] = ((word >> 16) & 0xFFFF);
          wf.second[idx_in_wf + 2] = ((word >> 32) & 0xFFFF);
          wf.second[idx_in_wf + 3] = ((word >> 48) & 0xFFFF);
          idx_in_data++;
        }
      }
    }

    return true;
  }
} VXMidasEvent;

int main(int argc, char* argv[]){
  // Read filename from command line
  if (argc != 2) {
    std::cout << "Usage: dump_vx2740_data <filename>" << std::endl;
    return 1;
  }

  char* filename = argv[1];

  TMReaderInterface* reader = TMNewReader(filename);

  if (reader->fError){
    std::cout << "Error opening file" << std::endl;
    return 1;
  }

  while(true){
    TMEvent* event = TMReadEvent(reader);

    if (!event){
      std::cout << "No more events left" << std::endl;
      break;
    } else if (event->error){
      std::cout << "Error reading event!" << std::endl;
      break;
    }

    event->FindAllBanks();

    for (auto& bank : event->banks) {
      if (bank.name[0] != 'D') {
        // Not a data bank
        continue;
      }

      VXMidasEvent parsed;
      if (!parsed.decode(event, bank)) {
        // Not a normal data event
        continue;
      }

      printf("Midas event number %d, VX event counter %d, FE %d, board %d, has data from %u channels (each %d samples long):\n", parsed.midas_serial_number, parsed.vx_event_counter, parsed.fe_idx, parsed.bd_idx, (int)parsed.waveforms.size(), (int)parsed.waveforms.begin()->second.size());
      
      for (auto& wf : parsed.waveforms) {
        int chan = wf.first;
        std::vector<uint16_t>& samples = wf.second;

        printf("> Chan %d: [%u, %u, %u, .....]\n", chan, samples[0], samples[1], samples[2]);
      }
    }
  }

  return 0;
}
#include "stdio.h"
#include "vx2740_wrapper.h"
#include "caen_exceptions.h"
#include <string>
#include <cstring>
#include <unistd.h>
#include <sys/time.h>
#include "fe_utils.h"

void usage(char *prog_name) {
   printf("Lets you get/set VX2740 parameters and user registers\n\n");
   printf("Usage: %s <hostname> get <param_path_or_register>\n", prog_name);
   printf("       %s <hostname> set <param_path_or_register_or_register_range> <value>\n", prog_name);
   printf("       %s <hostname> speedtest <param_path_or_register> <value_1> <value_2> <number_of_times>\n", prog_name);
   printf("       %s <hostname> swtrig <number_of_sw_triggers_to_issue>\n", prog_name);
   printf("       %s <hostname> tsm <number_of_tsm_triggers_to_issue>\n", prog_name);
   printf("       %s <hostname> dump_test_mem <channel>\n", prog_name);
   printf("       %s <hostname> [start_acq|stop_acq|reset|clear_data|start_stop_many]\n", prog_name);
   printf("E.g. : %s vx02 get /par/itlamajoritylev\n", prog_name);
   printf("E.g. : %s vx02 set /par/itlamajoritylev 3\n", prog_name);
   printf("E.g. : %s vx02 get 0x100\n", prog_name);
   printf("E.g. : %s vx02 set 0x100 255\n", prog_name);
   printf("E.g. : %s vx02 set 0x100 0xFF\n", prog_name);
   printf("E.g. : %s vx02 set 0x700-0x7fc 0xFF\n", prog_name);
   printf("E.g. : %s vx02 speedtest 0x100 0x1 0x2 100\n", prog_name);
   printf("E.g. : %s vx02 swtrig 3\n", prog_name);
   printf("E.g. : %s vx02 tsm 3\n", prog_name);
   printf("E.g. : %s vx02 dump_test_mem 2\n", prog_name);
   printf("E.g. : %s vx02 start_acq\n", prog_name);
   printf("E.g. : %s vx02 stop_acq\n", prog_name);
   printf("E.g. : %s vx02 start_stop_many\n", prog_name);
   printf("E.g. : %s vx02 reset\n", prog_name);
   printf("E.g. : %s vx02 clear_data\n", prog_name);
}

int main(int argc, char **argv) {
   if (argc < 3) {
      usage(argv[0]);
      return 0;
   }

   std::string hostname(argv[1]);
   std::string command(argv[2]);
   std::string path = argc > 3 ? std::string(argv[3]) : "";

   uint32_t reg_num_lower = 0;
   uint32_t reg_num_upper = 0;
   bool is_reg = (path.find("0x") == 0);

   if (is_reg) {
      int n_parsed = sscanf(argv[3], "0x%x-0x%x", &reg_num_lower, &reg_num_upper);

      if (n_parsed != 2) {
         n_parsed = sscanf(argv[3], "0x%x", &reg_num_lower);

         if (n_parsed == 1) {
            // Single register number
            reg_num_upper = reg_num_lower;
         } else {
            printf("Failed to parse register number/range.\n");
            usage(argv[0]);
            return 0;
         }
      }
   }

   VX2740 vx;
   vx.connect(argv[1], false, true);

   if (command == "get") {
      // Read value
      if (argc != 4) {
         usage(argv[0]);
         return 0;
      }

      if (is_reg) {
         for (int reg_num = reg_num_lower; reg_num <= reg_num_upper; reg_num += 4) {
            vx.params().print_user_register(reg_num);
         }
      } else {
         vx.params().print_param(path);
      }
   } else if (strcmp(argv[2], "set") == 0) {
      // Set, then readback
      if (argc != 5) {
         usage(argv[0]);
         return 0;
      }

      if (is_reg) {
         uint32_t set_val = 0;
         int n_parsed = sscanf(argv[4], "0x%x", &set_val);

         if (n_parsed == 0) {
            n_parsed = sscanf(argv[4], "%u", &set_val);

            if (n_parsed != 1) {
               printf("Failed to parse new register value.\n");
               usage(argv[0]);
               return 0;
            }
         }

         try {
            for (int reg_num = reg_num_lower; reg_num <= reg_num_upper; reg_num += 4) {
               vx.params().set_user_register(reg_num, set_val);
               vx.params().print_user_register(reg_num);
            }
         } catch(CaenException& e) {
            printf("Failed to set new register value: %s\n", e.what());
         }
      } else {
         try {
            vx.params().set(path, argv[4]);
            vx.params().print_param(path);
         } catch(CaenException& e) {
            printf("Failed to set new parameter value: %s\n", e.what());
         }
      }
   } else if (strcmp(argv[2], "speedtest") == 0) {
      // Set multiple times
      if (argc != 7) {
         usage(argv[0]);
         return 0;
      }

      uint32_t set_val_1 = 0;
      uint32_t set_val_2 = 0;
      uint32_t n_reps = 0;
      int n_parsed_3 = sscanf(argv[6], "0x%x", &n_reps);

      if (n_parsed_3 == 0) {
         n_parsed_3 = sscanf(argv[6], "%u", &n_reps);

         if (n_parsed_3 != 1) {
            printf("Failed to parse number of reps.\n");
            usage(argv[0]);
            return 0;
         }
      }

      if (n_reps % 2 == 1) {
         n_reps++;
      }

      if (is_reg) {
         int n_parsed_1 = sscanf(argv[4], "0x%x", &set_val_1);
         int n_parsed_2 = sscanf(argv[5], "0x%x", &set_val_2);

         if (n_parsed_1 == 0) {
            n_parsed_1 = sscanf(argv[4], "%u", &set_val_1);

            if (n_parsed_1 != 1) {
               printf("Failed to parse first register value.\n");
               usage(argv[0]);
               return 0;
            }
         }
         if (n_parsed_2 == 0) {
            n_parsed_2 = sscanf(argv[5], "%u", &set_val_2);

            if (n_parsed_2 != 1) {
               printf("Failed to parse second register value.\n");
               usage(argv[0]);
               return 0;
            }
         }
      } else {

         try {
            
            
         } catch(CaenException& e) {
            printf("Failed to set new parameter value: %s\n", e.what());
         }
      }

      timeval start, end;
      gettimeofday(&start, NULL);
      uint32_t rdb = 0;
      std::string rdb_s;

      try {
         for (uint32_t i = 0; i < n_reps; i += 2) {
            if (is_reg) {
               vx.params().set_user_register(reg_num_lower, set_val_1);
               vx.params().get_user_register(reg_num_lower, rdb);
               if (rdb != set_val_1) {
                  printf("Fail!\n");
               }
               vx.params().set_user_register(reg_num_lower, set_val_2);
               vx.params().get_user_register(reg_num_lower, rdb);
               if (rdb != set_val_2) {
                  printf("Fail!\n");
               }
            } else {
               vx.params().set(path, argv[4]);
               vx.params().get(path, rdb_s);
               vx.params().set(path, argv[5]);
               vx.params().get(path, rdb_s);
            }
         }
      } catch(CaenException& e) {
         printf("Failed to set new value: %s\n", e.what());
      }

      gettimeofday(&end, NULL);

      double delta_secs = (end.tv_sec - start.tv_sec) + 1e-6 * (end.tv_usec - start.tv_usec);
      printf("\nTook %.6lfs to set value %u times => %.6lfs per value => %.2lfkHz\n", delta_secs, n_reps, delta_secs/n_reps, 1e-3 * (n_reps/delta_secs));
   
   } else if (command == "swtrig")  {
      if (argc != 4) {
         usage(argv[0]);
         return 0;
      }

      uint32_t n_trig = 0;
      int n_parsed = sscanf(argv[3], "%u", &n_trig);

      if (n_parsed != 1) {
         printf("Failed to parse number of software triggers to issue.\n");
         usage(argv[0]);
         return 0;
      }

      for (uint32_t i = 0; i < n_trig; i++) {
         // Issue triggers at 100ms intervals
         printf("Issuing trigger %u\n", i);
         vx.commands().issue_software_trigger();
         usleep(100000);
      }
   } else if (command == "tsm")  {
      if (argc != 4) {
         usage(argv[0]);
         return 0;
      }

      uint32_t n_trig = 0;
      int n_parsed = sscanf(argv[3], "%u", &n_trig);

      if (n_parsed != 1) {
         printf("Failed to parse number of TSM triggers to issue.\n");
         usage(argv[0]);
         return 0;
      }

      std::string fw_type, fw_ver;
      vx.params().get_firmware_type(fw_type);
      vx.params().get_firmware_version(fw_ver);

      if (fw_type == "Scope" || fe_utils::is_fw_version_before(fw_ver, "2023032806")) {
         printf("TSM triggers only available in OPEN_DPP FW since ver 2023032806. You are running %s ver %s.\n", fw_type.c_str(), fw_ver.c_str());
         return 0;
      }

      for (uint32_t i = 0; i < n_trig; i++) {
         // Issue triggers at 1000ms intervals
         printf("Issuing TSM trigger %u\n", i);
         vx.params().set_user_register_bit(0x5C, 2, true); // Self-clearing
         usleep(1000000);
      }
   } else if (command == "dump_test_mem") {
      if (argc != 4) {
         usage(argv[0]);
         return 0;
      }

      uint32_t chan = 0;
      int n_parsed = sscanf(argv[3], "%u", &chan);

      if (n_parsed != 1 || chan > 63) {
         printf("Failed to parse channel number to read.\n");
         usage(argv[0]);
         return 0;
      }

      vx.params().set_user_register(0x94,chan); 

      for (int i = 0; i < 1024; i++) {
         vx.params().set_user_register(0x90,i<<16);
         uint32_t val = 0;
         vx.params().get_user_register(0x98,val); 
         if (i % 8 == 0) {
            printf("\n%04d: ", i);
         }
         printf("0x%04x ", val);
      }
      printf("\n");
   } else if (command == "start_acq") {
      vx.commands().start_acq(vx.params().is_sw_start_enabled());
   } else if (command == "stop_acq") {
      vx.commands().stop_acq();
   } else if (command == "start_stop_many") {
      int max_tries = 100;

      for (int i = 1; i <= max_tries; i++) {
         fe_utils::ts_printf("Arming %d/%d...\n", i, max_tries);
         vx.commands().start_acq(vx.params().is_sw_start_enabled());
         fe_utils::ts_printf("Armed...\n");
         usleep(1e6);
         fe_utils::ts_printf("Stopping...\n");
         vx.commands().stop_acq();
         fe_utils::ts_printf("Stopped...\n");
         usleep(1e5);
      }
   } else if (command == "reset") {
      vx.commands().reset();
   } else if (command == "clear_data") {
      vx.commands().clear_data();
   } else {
      printf("Invalid command '%s'. Specify get/set/swtrig/tsm/start_acq/stop_acq/reset/clear_data.\n", command.c_str());
      usage(argv[0]);
      return 0;
   }

   return 0;
}
